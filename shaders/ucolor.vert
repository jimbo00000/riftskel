// ucolor.vert
#version 330

in vec3 vPosition;

uniform mat4 mvmtx;
uniform mat4 prmtx;

void main()
{
    gl_Position = prmtx * mvmtx * vec4(vPosition, 1.);
}
