# RiftSkel

The [RiftSkeleton](https://github.com/jimbo00000/RiftSkeleton) project got over-complex from trying to handle disparate API versions. I let it go stale for a few months and forgot how it worked, lost the structure of it in my head.

This is a minimal rewrite to keep it as small as possible. The IScene interface is still there and old subclasses of Scene will still work, but now all the setup code is in one main file of <500 lines containing glfw and OVR code.


## Building luajit on Windows
 - `git submodule update`
 - Press the windows key and type `visual studio tools`
 - Launch `Developer Command Prompt for VS2013`
 - `cd` to `luajit-2.0/src` (you'll need the absolute path)
 - `msvcbuild.bat`
