
jim@mahler:~$ ls Downloads/
fglrx_15.201-0ubuntu1_amd64_UB_14.01.deb  glfw-3.1.2.zip
glfw-3.1.2                                ovr_sdk_linux_0.4.4.tar.xz
jim@mahler:~$ sudo apt-get install glew
[sudo] password for jim: 
Reading package lists... Done
Building dependency tree       
Reading state information... Done
E: Unable to locate package glew
jim@mahler:~$ sudo apt-get install libglew-dev
Reading package lists... Done
Building dependency tree       
Reading state information... Done
The following NEW packages will be installed:
  libglew-dev
0 upgraded, 1 newly installed, 0 to remove and 314 not upgraded.
Need to get 109 kB of archives.
After this operation, 1,113 kB of additional disk space will be used.
Get:1 http://us.archive.ubuntu.com/ubuntu/ vivid/main libglew-dev amd64 1.10.0-3 [109 kB]
Fetched 109 kB in 0s (1,037 kB/s)
Selecting previously unselected package libglew-dev:amd64.
(Reading database ... 181323 files and directories currently installed.)
Preparing to unpack .../libglew-dev_1.10.0-3_amd64.deb ...
Unpacking libglew-dev:amd64 (1.10.0-3) ...
Setting up libglew-dev:amd64 (1.10.0-3) ...
jim@mahler:~$ sudo apt-get install libglm-dev
Reading package lists... Done
Building dependency tree       
Reading state information... Done
The following NEW packages will be installed:
  libglm-dev
0 upgraded, 1 newly installed, 0 to remove and 314 not upgraded.
Need to get 148 kB of archives.
After this operation, 1,956 kB of additional disk space will be used.
Get:1 http://us.archive.ubuntu.com/ubuntu/ vivid/main libglm-dev all 0.9.5.4-1 [148 kB]
Fetched 148 kB in 0s (1,104 kB/s)
Selecting previously unselected package libglm-dev.
(Reading database ... 181331 files and directories currently installed.)
Preparing to unpack .../libglm-dev_0.9.5.4-1_all.deb ...
Unpacking libglm-dev (0.9.5.4-1) ...
Setting up libglm-dev (0.9.5.4-1) ...







jim@mahler:~$ ls
code  Desktop  Documents  Downloads  examples.desktop  Music  Pictures  Public  Templates  Videos
jim@mahler:~$ cd code/
jim@mahler:~/code$ ls
git  hg
jim@mahler:~/code$ cd git/
jim@mahler:~/code/git$ ls
opengl-with-luajit  opengl-with-python
jim@mahler:~/code/git$ cd opengl-with-luajit/
jim@mahler:~/code/git/opengl-with-luajit$ git pull
remote: Counting objects: 502, done.
remote: Compressing objects: 100% (502/502), done.
remote: Total 502 (delta 344), reused 0 (delta 0)
Receiving objects: 100% (502/502), 5.53 MiB | 2.28 MiB/s, done.
Resolving deltas: 100% (344/344), completed with 16 local objects.
From http://bitbucket.org/jimbo00000/opengl-with-luajit
   ce861f9..3f4be2a  master     -> origin/master
Updating ce861f9..3f4be2a
Fast-forward
 data/myDrawing.obj                       |   20 +
 data/myDrawing1.obj                      |  945 +++++++++++++++++++++++++
 data/myDrawing2.obj                      |  817 +++++++++++++++++++++
 data/myDrawing3.obj                      | 1428 +++++++++++++++++++++++++++++++++++++
 data/negx.jpg                            |  Bin 0 -> 1039406 bytes
 data/negx_128.raw                        | 1045 +++++++++++++++++++++++++++
 data/negy.jpg                            |  Bin 0 -> 994070 bytes
 data/negy_128.raw                        |  232 ++++++
 data/negz.jpg                            |  Bin 0 -> 911570 bytes
 data/negz_128.raw                        | 1006 ++++++++++++++++++++++++++
 data/posx.jpg                            |  Bin 0 -> 1036660 bytes
 data/posx_128.raw                        |  698 ++++++++++++++++++
 data/posy.jpg                            |  Bin 0 -> 631474 bytes
 data/posy_128.raw                        | 2163 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 data/posz.jpg                            |  Bin 0 -> 871498 bytes
 data/posz_128.raw                        | 1533 +++++++++++++++++++++++++++++++++++++++
 data/readme.txt                          |   13 +
 data/stone_128x128.raw                   |    1 +
 main_glfw.lua                            |  692 +++++++++---------
 scene/clockface.lua                      |  157 ++++
 scene/comp_scene.lua                     |    4 +-
 scene/compute_mesh.lua                   |    2 +-
 scene/cubescene.lua                      |   15 +-
 scene/cubescene2.lua                     |   24 +-
 scene/cubeswarm.lua                      |   39 +
 scene/gamescene.lua                      |  143 +++-
 scene/grid_scene.lua                     |  920 ++++++++++++------------
 scene/gridcube_scene.lua                 |    2 +-
 scene/hybrid_scene.lua                   |   29 +-
 scene/lineobj.lua                        |   52 ++
 scene/lineobjscene.lua                   |  140 ++++
 scene/moon_scene.lua                     |    2 +-
 scene/moon_scene2.lua                    |  521 --------------
 scene/moon_scene3.lua                    |   11 +-
 scene/nbody06.lua                        |  383 ++++++++++
 scene/particles_scene.lua                |  376 +++++++---
 scene/points.lua                         |  175 +++++
 scene/sdf_mesh_scene.lua                 |  213 ++++++
 scene/shadertoy_scene2.lua               |   23 +-
 scene/simplecube_scene.lua               |   50 +-
 scene/subdivided_cube.lua                |    2 +-
 scene/tess_scene.lua                     |   16 +-
 scene/texturedcube.lua                   |  217 ++++++
 scene/texturedcubemap.lua                |  209 ++++++
 scene/{wirecubescene.lua => vsfstri.lua} |   53 +-
 tools/graphdeps.py                       |   23 +
 util/matrixmath.lua                      |   20 +
 47 files changed, 12895 insertions(+), 1519 deletions(-)
 create mode 100644 data/myDrawing.obj
 create mode 100644 data/myDrawing1.obj
 create mode 100644 data/myDrawing2.obj
 create mode 100644 data/myDrawing3.obj
 create mode 100644 data/negx.jpg
 create mode 100644 data/negx_128.raw
 create mode 100644 data/negy.jpg
 create mode 100644 data/negy_128.raw
 create mode 100644 data/negz.jpg
 create mode 100644 data/negz_128.raw
 create mode 100644 data/posx.jpg
 create mode 100644 data/posx_128.raw
 create mode 100644 data/posy.jpg
 create mode 100644 data/posy_128.raw
 create mode 100644 data/posz.jpg
 create mode 100644 data/posz_128.raw
 create mode 100644 data/readme.txt
 create mode 100644 data/stone_128x128.raw
 create mode 100644 scene/clockface.lua
 create mode 100644 scene/cubeswarm.lua
 create mode 100644 scene/lineobj.lua
 create mode 100644 scene/lineobjscene.lua
 delete mode 100644 scene/moon_scene2.lua
 create mode 100644 scene/nbody06.lua
 create mode 100644 scene/points.lua
 create mode 100644 scene/sdf_mesh_scene.lua
 create mode 100644 scene/texturedcube.lua
 create mode 100644 scene/texturedcubemap.lua
 rename scene/{wirecubescene.lua => vsfstri.lua} (73%)
 create mode 100644 tools/graphdeps.py
jim@mahler:~/code/git/opengl-with-luajit$ ./luajit main_glfw.lua 
./luajit: ./glfw.lua:12: libglfw.so.3.1: cannot open shared object file: No such file or directory
stack traceback:
	[C]: in function 'load'
	./glfw.lua:12: in main chunk
	[C]: in function 'require'
	main_glfw.lua:4: in main chunk
	[C]: at 0x00404760
jim@mahler:~/code/git/opengl-with-luajit$ ls
data           glfw.lua           lua.dll         luajit-sol             scene
effect         jit                luajit          main_glfw.lua          segoe_ui128_0.raw
FlameGraph     libglfw.3.1.dylib  luajit-2.1.exe  minimal_main_glfw.lua  segoe_ui128.fnt
get_stats.bat  libglfw.so.3.1     luajit.exe      opengl.lua             tools
glfw3.dll      lua51.dll          luajit-osx      README.md              util
jim@mahler:~/code/git/opengl-with-luajit$ LD_LIBRARY+PATH=./ ./luajit main_glfw.lua 
bash: LD_LIBRARY+PATH=./: No such file or directory
jim@mahler:~/code/git/opengl-with-luajit$ LD_LIBRARY_PATH=./ ./luajit main_glfw.lua 
Loading obj data/myDrawing3.obj
Object	492	0
Object	182	493
Object	395	676
Object	46	1072
Object	298	1119
5
scene.vsfstri initGL: 0ms
scene.clockface initGL: 0ms
scene.points initGL: 13ms
scene.texturedcube initGL: 0ms
scene.texturedcubemap initGL: 5ms
MAX_COMPUTE_WORK_GROUP_COUNT
0	65535
1	65535
2	65535
num_verts	396294	1548.0234375
num_tri_idxs	2359296	9216
scene.moon_scene3 initGL: 9ms
MAX_COMPUTE_WORK_GROUP_COUNT
0	65535
1	65535
2	65535
num_verts	396294	1548.0234375
num_tri_idxs	2359296	9216
scene.sdf_mesh_scene initGL: 231ms
scene.comp_scene initGL: 5ms
scene.cubescene initGL: 1ms
scene.compute_mesh initGL: 71ms
scene.cubescene initGL: 1ms
jim@mahler:~/code/git/opengl-with-luajit$ cd ..
(reverse-i-search)`': git clone^C
jim@mahler:~/code/git$ ^C
jim@mahler:~/code/git$ ^C
jim@mahler:~/code/git$ ^C
jim@mahler:~/code/git$ git clone http://bitbucket.org/jimbo00000/riftskel.git
Cloning into 'riftskel'...
remote: Counting objects: 1278, done.
remote: Compressing objects: 100% (1270/1270), done.
remote: Total 1278 (delta 846), reused 0 (delta 0)
Receiving objects: 100% (1278/1278), 773.91 KiB | 0 bytes/s, done.
Resolving deltas: 100% (846/846), done.
Checking connectivity... done.
jim@mahler:~/code/git$ cd riftskel/
jim@mahler:~/code/git/riftskel$ cmake-gui . &\
> 
[1] 3419
jim@mahler:~/code/git/riftskel$ cd build/
jim@mahler:~/code/git/riftskel/build$ make
Scanning dependencies of target Util
[  7%] Building CXX object CMakeFiles/Util.dir/src/Util/Logger.cpp.o
[ 14%] Building CXX object CMakeFiles/Util.dir/src/Util/StringFunctions.cpp.o
[ 21%] Building CXX object CMakeFiles/Util.dir/src/Util/FPSTimer.cpp.o
[ 28%] Building CXX object CMakeFiles/Util.dir/src/Util/GL/ShaderWithVariables.cpp.o
In file included from /home/jim/code/git/riftskel/src/Util/GL/ShaderWithVariables.cpp:3:0:
/home/jim/code/git/riftskel/src/Util/GL/ShaderWithVariables.h:11:21: fatal error: GL/glew.h: No such file or directory
 #include <GL/glew.h>
                     ^
compilation terminated.
CMakeFiles/Util.dir/build.make:123: recipe for target 'CMakeFiles/Util.dir/src/Util/GL/ShaderWithVariables.cpp.o' failed
make[2]: *** [CMakeFiles/Util.dir/src/Util/GL/ShaderWithVariables.cpp.o] Error 1
CMakeFiles/Makefile2:131: recipe for target 'CMakeFiles/Util.dir/all' failed
make[1]: *** [CMakeFiles/Util.dir/all] Error 2
Makefile:76: recipe for target 'all' failed
make: *** [all] Error 2
jim@mahler:~/code/git/riftskel/build$ make
[  7%] Building CXX object CMakeFiles/Util.dir/src/Util/GL/ShaderWithVariables.cpp.o
[ 14%] Building CXX object CMakeFiles/Util.dir/src/Util/GL/MatrixFunctions.cpp.o
/home/jim/code/git/riftskel/src/Util/GL/MatrixFunctions.cpp:3:23: fatal error: glm/glm.hpp: No such file or directory
 #include <glm/glm.hpp>
                       ^
compilation terminated.
CMakeFiles/Util.dir/build.make:146: recipe for target 'CMakeFiles/Util.dir/src/Util/GL/MatrixFunctions.cpp.o' failed
make[2]: *** [CMakeFiles/Util.dir/src/Util/GL/MatrixFunctions.cpp.o] Error 1
CMakeFiles/Makefile2:131: recipe for target 'CMakeFiles/Util.dir/all' failed
make[1]: *** [CMakeFiles/Util.dir/all] Error 2
Makefile:76: recipe for target 'all' failed
make: *** [all] Error 2
jim@mahler:~/code/git/riftskel/build$ make
[  7%] Building CXX object CMakeFiles/Util.dir/src/Util/GL/MatrixFunctions.cpp.o
/home/jim/code/git/riftskel/src/Util/GL/MatrixFunctions.cpp:8:22: fatal error: OVR_CAPI.h: No such file or directory
 #include <OVR_CAPI.h>
                      ^
compilation terminated.
CMakeFiles/Util.dir/build.make:146: recipe for target 'CMakeFiles/Util.dir/src/Util/GL/MatrixFunctions.cpp.o' failed
make[2]: *** [CMakeFiles/Util.dir/src/Util/GL/MatrixFunctions.cpp.o] Error 1
CMakeFiles/Makefile2:131: recipe for target 'CMakeFiles/Util.dir/all' failed
make[1]: *** [CMakeFiles/Util.dir/all] Error 2
Makefile:76: recipe for target 'all' failed
make: *** [all] Error 2
jim@mahler:~/code/git/riftskel/build$ make
\Scanning dependencies of target Scene
[  7%] Building CXX object CMakeFiles/Scene.dir/src/Scene/LineObjScene.cpp.o
[ 14%] Building CXX object CMakeFiles/Scene.dir/src/Scene/LineObj.cpp.o
[ 21%] Building CXX object CMakeFiles/Scene.dir/src/Scene/Scene.cpp.o
Linking CXX static library libScene.a
[ 21%] Built target Scene
Scanning dependencies of target Util
[ 28%] Building CXX object CMakeFiles/Util.dir/src/Util/GL/MatrixFunctions.cpp.o
/home/jim/code/git/riftskel/src/Util/GL/MatrixFunctions.cpp:8:22: fatal error: OVR_CAPI.h: No such file or directory
 #include <OVR_CAPI.h>
                      ^
compilation terminated.
CMakeFiles/Util.dir/build.make:146: recipe for target 'CMakeFiles/Util.dir/src/Util/GL/MatrixFunctions.cpp.o' failed
make[2]: *** [CMakeFiles/Util.dir/src/Util/GL/MatrixFunctions.cpp.o] Error 1
CMakeFiles/Makefile2:131: recipe for target 'CMakeFiles/Util.dir/all' failed
make[1]: *** [CMakeFiles/Util.dir/all] Error 2
Makefile:76: recipe for target 'all' failed
make: *** [all] Error 2
jim@mahler:~/code/git/riftskel/build$ \^C
jim@mahler:~/code/git/riftskel/build$ ^C
jim@mahler:~/code/git/riftskel/build$ make clean
jim@mahler:~/code/git/riftskel/build$ make
[  7%] Building CXX object CMakeFiles/Scene.dir/src/Scene/LineObjScene.cpp.o
[ 14%] Building CXX object CMakeFiles/Scene.dir/src/Scene/LineObj.cpp.o
[ 21%] Building CXX object CMakeFiles/Scene.dir/src/Scene/Scene.cpp.o
Linking CXX static library libScene.a
[ 21%] Built target Scene
[ 28%] Building CXX object CMakeFiles/Util.dir/src/Util/Logger.cpp.o
[ 35%] Building CXX object CMakeFiles/Util.dir/src/Util/StringFunctions.cpp.o
[ 42%] Building CXX object CMakeFiles/Util.dir/src/Util/FPSTimer.cpp.o
[ 50%] Building CXX object CMakeFiles/Util.dir/src/Util/GL/ShaderWithVariables.cpp.o
[ 57%] Building CXX object CMakeFiles/Util.dir/src/Util/GL/MatrixFunctions.cpp.o
/home/jim/code/git/riftskel/src/Util/GL/MatrixFunctions.cpp:8:22: fatal error: OVR_CAPI.h: No such file or directory
 #include <OVR_CAPI.h>
                      ^
compilation terminated.
CMakeFiles/Util.dir/build.make:146: recipe for target 'CMakeFiles/Util.dir/src/Util/GL/MatrixFunctions.cpp.o' failed
make[2]: *** [CMakeFiles/Util.dir/src/Util/GL/MatrixFunctions.cpp.o] Error 1
CMakeFiles/Makefile2:131: recipe for target 'CMakeFiles/Util.dir/all' failed
make[1]: *** [CMakeFiles/Util.dir/all] Error 2
Makefile:76: recipe for target 'all' failed
make: *** [all] Error 2
jim@mahler:~/code/git/riftskel/build$ make clean
jim@mahler:~/code/git/riftskel/build$ make
Scanning dependencies of target Scene
[  7%] Building CXX object CMakeFiles/Scene.dir/src/Scene/LineObjScene.cpp.o
[ 14%] Building CXX object CMakeFiles/Scene.dir/src/Scene/LineObj.cpp.o
[ 21%] Building CXX object CMakeFiles/Scene.dir/src/Scene/Scene.cpp.o
Linking CXX static library libScene.a
[ 21%] Built target Scene
Scanning dependencies of target Util
[ 28%] Building CXX object CMakeFiles/Util.dir/src/Util/Logger.cpp.o
[ 35%] Building CXX object CMakeFiles/Util.dir/src/Util/StringFunctions.cpp.o
[ 42%] Building CXX object CMakeFiles/Util.dir/src/Util/FPSTimer.cpp.o
[ 50%] Building CXX object CMakeFiles/Util.dir/src/Util/GL/ShaderWithVariables.cpp.o
[ 57%] Building CXX object CMakeFiles/Util.dir/src/Util/GL/MatrixFunctions.cpp.o
/home/jim/code/git/riftskel/src/Util/GL/MatrixFunctions.cpp:8:22: fatal error: OVR_CAPI.h: No such file or directory
 #include <OVR_CAPI.h>
                      ^
compilation terminated.
CMakeFiles/Util.dir/build.make:146: recipe for target 'CMakeFiles/Util.dir/src/Util/GL/MatrixFunctions.cpp.o' failed
make[2]: *** [CMakeFiles/Util.dir/src/Util/GL/MatrixFunctions.cpp.o] Error 1
CMakeFiles/Makefile2:131: recipe for target 'CMakeFiles/Util.dir/all' failed
make[1]: *** [CMakeFiles/Util.dir/all] Error 2
Makefile:76: recipe for target 'all' failed
make: *** [all] Error 2
jim@mahler:~/code/git/riftskel/build$ make
Scanning dependencies of target Scene
[  7%] Building CXX object CMakeFiles/Scene.dir/src/Scene/LineObjScene.cpp.o
[ 14%] Building CXX object CMakeFiles/Scene.dir/src/Scene/LineObj.cpp.o
[ 21%] Building CXX object CMakeFiles/Scene.dir/src/Scene/Scene.cpp.o
Linking CXX static library libScene.a
[ 21%] Built target Scene
Scanning dependencies of target Util
[ 28%] Building CXX object CMakeFiles/Util.dir/src/Util/Logger.cpp.o
[ 35%] Building CXX object CMakeFiles/Util.dir/src/Util/StringFunctions.cpp.o
[ 42%] Building CXX object CMakeFiles/Util.dir/src/Util/FPSTimer.cpp.o
[ 50%] Building CXX object CMakeFiles/Util.dir/src/Util/GL/ShaderWithVariables.cpp.o
[ 57%] Building CXX object CMakeFiles/Util.dir/src/Util/GL/MatrixFunctions.cpp.o
[ 64%] Building CXX object CMakeFiles/Util.dir/src/Util/GL/FBO.cpp.o
[ 71%] Building CXX object CMakeFiles/Util.dir/src/Util/GL/GLUtils.cpp.o
[ 78%] Building CXX object CMakeFiles/Util.dir/src/Util/GL/ShaderFunctions.cpp.o
[ 85%] Building CXX object CMakeFiles/Util.dir/src/Util/DirectoryFunctions.cpp.o
[ 92%] Building CXX object CMakeFiles/Util.dir/src/Util/Timer.cpp.o
Linking CXX static library libUtil.a
[ 92%] Built target Util
Scanning dependencies of target RiftSkel
[100%] Building CXX object CMakeFiles/RiftSkel.dir/src/main_glfw_ovrsdk05.cpp.o
/home/jim/code/git/riftskel/src/main_glfw_ovrsdk05.cpp:23:17: fatal error: OVR.h: No such file or directory
 #include <OVR.h>
                 ^
compilation terminated.
CMakeFiles/RiftSkel.dir/build.make:54: recipe for target 'CMakeFiles/RiftSkel.dir/src/main_glfw_ovrsdk05.cpp.o' failed
make[2]: *** [CMakeFiles/RiftSkel.dir/src/main_glfw_ovrsdk05.cpp.o] Error 1
CMakeFiles/Makefile2:61: recipe for target 'CMakeFiles/RiftSkel.dir/all' failed
make[1]: *** [CMakeFiles/RiftSkel.dir/all] Error 2
Makefile:76: recipe for target 'all' failed
make: *** [all] Error 2
jim@mahler:~/code/git/riftskel/build$ make
-- Invoking python tools/hardcode_shaders.py :
-- hardcode_shaders.py writing the following shaders to autogen/ :
    hardcoding shader: ucolor.frag
    hardcoding shader: basic.vert
    hardcoding shader: basic.frag
    hardcoding shader: basicplane.vert
    hardcoding shader: ucolor.vert
    hardcoding shader: basicplane.frag

-- 0
--  success.
CMAKE_SYSTEM_NAME: Linux
Using GLFW Framework.
Using AntTweakBar.
Removing OVRSDK-dependent files...
-- Configuring done
-- Generating done
-- Build files have been written to: /home/jim/code/git/riftskel/build
Scanning dependencies of target Util
[  7%] Building CXX object CMakeFiles/Util.dir/src/Util/GL/ShaderFunctions.cpp.o
Linking CXX static library libUtil.a
[ 71%] Built target Util
[ 92%] Built target Scene
Scanning dependencies of target RiftSkel
[100%] Building CXX object CMakeFiles/RiftSkel.dir/src/main_glfw_monitor.cpp.o
/home/jim/code/git/riftskel/src/main_glfw_monitor.cpp:22:27: fatal error: AntTweakBar.h: No such file or directory
 #  include <AntTweakBar.h>
                           ^
compilation terminated.
CMakeFiles/RiftSkel.dir/build.make:54: recipe for target 'CMakeFiles/RiftSkel.dir/src/main_glfw_monitor.cpp.o' failed
make[2]: *** [CMakeFiles/RiftSkel.dir/src/main_glfw_monitor.cpp.o] Error 1
CMakeFiles/Makefile2:61: recipe for target 'CMakeFiles/RiftSkel.dir/all' failed
make[1]: *** [CMakeFiles/RiftSkel.dir/all] Error 2
Makefile:76: recipe for target 'all' failed
make: *** [all] Error 2
jim@mahler:~/code/git/riftskel/build$ make
Scanning dependencies of target Scene
[  7%] Building CXX object CMakeFiles/Scene.dir/src/Scene/LineObjScene.cpp.o
[ 14%] Building CXX object CMakeFiles/Scene.dir/src/Scene/LineObj.cpp.o
[ 21%] Building CXX object CMakeFiles/Scene.dir/src/Scene/Scene.cpp.o
Linking CXX static library libScene.a
[ 21%] Built target Scene
Scanning dependencies of target Util
[ 28%] Building CXX object CMakeFiles/Util.dir/src/Util/Logger.cpp.o
[ 35%] Building CXX object CMakeFiles/Util.dir/src/Util/StringFunctions.cpp.o
[ 42%] Building CXX object CMakeFiles/Util.dir/src/Util/FPSTimer.cpp.o
[ 50%] Building CXX object CMakeFiles/Util.dir/src/Util/GL/ShaderWithVariables.cpp.o
[ 57%] Building CXX object CMakeFiles/Util.dir/src/Util/GL/MatrixFunctions.cpp.o
[ 64%] Building CXX object CMakeFiles/Util.dir/src/Util/GL/FBO.cpp.o
[ 71%] Building CXX object CMakeFiles/Util.dir/src/Util/GL/GLUtils.cpp.o
[ 78%] Building CXX object CMakeFiles/Util.dir/src/Util/GL/ShaderFunctions.cpp.o
[ 85%] Building CXX object CMakeFiles/Util.dir/src/Util/DirectoryFunctions.cpp.o
[ 92%] Building CXX object CMakeFiles/Util.dir/src/Util/Timer.cpp.o
Linking CXX static library libUtil.a
[ 92%] Built target Util
Scanning dependencies of target RiftSkel
[100%] Building CXX object CMakeFiles/RiftSkel.dir/src/main_glfw_monitor.cpp.o
/home/jim/code/git/riftskel/src/main_glfw_monitor.cpp:79:21: error: expected initializer before ‘GoToSceneCB’
 static void TW_CALL GoToSceneCB(void *clientData)
                     ^
CMakeFiles/RiftSkel.dir/build.make:54: recipe for target 'CMakeFiles/RiftSkel.dir/src/main_glfw_monitor.cpp.o' failed
make[2]: *** [CMakeFiles/RiftSkel.dir/src/main_glfw_monitor.cpp.o] Error 1
CMakeFiles/Makefile2:61: recipe for target 'CMakeFiles/RiftSkel.dir/all' failed
make[1]: *** [CMakeFiles/RiftSkel.dir/all] Error 2
Makefile:76: recipe for target 'all' failed
make: *** [all] Error 2
jim@mahler:~/code/git/riftskel/build$ make
[ 21%] Built target Scene
[ 92%] Built target Util
Scanning dependencies of target RiftSkel
[100%] Building CXX object CMakeFiles/RiftSkel.dir/src/main_glfw_monitor.cpp.o
/home/jim/code/git/riftskel/src/main_glfw_monitor.cpp: In function ‘int main(int, char**)’:
/home/jim/code/git/riftskel/src/main_glfw_monitor.cpp:419:44: error: invalid conversion from ‘void (*)(GLenum, GLenum, GLuint, GLenum, GLsizei, const GLchar*, const void*) {aka void (*)(unsigned int, unsigned int, unsigned int, unsigned int, int, const char*, const void*)}’ to ‘GLDEBUGPROC {aka void (*)(unsigned int, unsigned int, unsigned int, unsigned int, int, const char*, void*)}’ [-fpermissive]
     glDebugMessageCallback(myCallback, NULL);
                                            ^
CMakeFiles/RiftSkel.dir/build.make:54: recipe for target 'CMakeFiles/RiftSkel.dir/src/main_glfw_monitor.cpp.o' failed
make[2]: *** [CMakeFiles/RiftSkel.dir/src/main_glfw_monitor.cpp.o] Error 1
CMakeFiles/Makefile2:61: recipe for target 'CMakeFiles/RiftSkel.dir/all' failed
make[1]: *** [CMakeFiles/RiftSkel.dir/all] Error 2
Makefile:76: recipe for target 'all' failed
make: *** [all] Error 2
jim@mahler:~/code/git/riftskel/build$ make
[ 21%] Built target Scene
[ 92%] Built target Util
Scanning dependencies of target RiftSkel
[100%] Building CXX object CMakeFiles/RiftSkel.dir/src/main_glfw_monitor.cpp.o
Linking CXX executable RiftSkel
[100%] Built target RiftSkel
jim@mahler:~/code/git/riftskel/build$ ./RiftSkel 

Shader [basic] 
  vs-<<file>> OK   fs-<<file>> OK   prog: OK 
  vars: vs-<<file>>  fs-<<file>> 2 unis, 3 attrs.

Shader [basicplane] 
  vs-<<file>> OK   fs-<<file>> OK   prog: OK 
  vars: vs-<<file>>  fs-<<file>> 2 unis, 3 attrs.

