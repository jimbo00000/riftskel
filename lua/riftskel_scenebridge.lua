-- Bridge to call Lua scenes from C++

package.path = "../lua/?.lua;" .. package.path

local ffi = require("ffi")
local openGL = require("opengl")

local Scene = nil
local scenedir = "scene2"

function on_lua_setscene(name)
    local fullname = scenedir.."."..string.sub(name, 0, -5)
    print("Lua Setting the scene to "..fullname)
    if Scene and Scene.exitGL then
        Scene:exitGL()
    end

    package.loaded[name] = nil
    Scene = nil

    if not Scene then
        SceneLibrary = require(fullname)
        Scene = SceneLibrary.new()
        if Scene then
            --local now = clock()
            -- Instruct the scene where to load data from. Dir is relative to app's working dir.
            local dir = "../lua/data"
            if Scene.setDataDirectory then Scene:setDataDirectory(dir) end
            if Scene.setWindowSize then Scene:setWindowSize(win_w, win_h) end
            Scene:initGL()
            --local initTime = clock() - now
            --lastSceneChangeTime = now
            collectgarbage()
            --print(name,
            --    "init time: "..math.floor(1000*initTime).." ms",
            --    "memory: "..math.floor(collectgarbage("count")).." kB")
        end
    end
end

-- Cast the array cdata ptr(passes from glm::value_ptr(glm::mat4),
-- which gives a float[16]) to a table for further manipulation here in Lua.
function array_to_table(array)
    local m0 = ffi.cast("float*", array)
    -- The cdata array is 0-indexed. Here we clumsily jam it back
    -- into a Lua-style, 1-indexed table(array portion).
    local tab = {}
    for i=0,15 do tab[i+1] = m0[i] end
    return tab
end

function on_lua_draw(pmv, ppr)
    local mv = array_to_table(pmv)
    local pr = array_to_table(ppr)
    Scene:render_for_one_eye(mv, pr)
    if Scene.set_origin_matrix then Scene:set_origin_matrix(mv) end
end

function on_lua_initgl(pLoaderFunc)
    --[[
        Now, the GL function loading business...
        Everything in GL 1.2 or older has names in OpenGL32.dll/opengl32.dll (Windows),
        while pointers to all newer functions must be obtained from a loader function.
        Using the wglGetProcAddress provided by that dll will return NULL pointers for
        all the old functions. Using glfwGetProcAddress takes care of both cases, but
        pulling it in via the ffi would be a redundant copy of GLFW, and would require
        an init of the second GLFW, which it might not even do. Instead, just pass the
        pointer to to C++ app's GLFW's glfwGetProcAddress.

        https://www.opengl.org/wiki/Load_OpenGL_Functions
        https://www.opengl.org/wiki/Talk%3aPlatform_specifics%3a_Windows
        http://stackoverflow.com/questions/25214519/opengl-1-0-and-1-1-function-pointers-on-windows
    ]]
    ffi.cdef[[
    typedef void (*GLFWglproc)();
    GLFWglproc glfwGetProcAddress(const char* procname);
    typedef GLFWglproc (*GLFWGPAProc)(const char*);
    ]]
    openGL.loader = ffi.cast('GLFWGPAProc', pLoaderFunc)
    openGL:import()

    on_lua_setscene("vsfstri.lua")

    -- Instruct the scene where to load data from. Dir is relative to app's working dir.
    if Scene.setDataDirectory then Scene:setDataDirectory("../lua/data") end

    Scene:initGL()
end

function on_lua_exitgl()
    Scene:exitGL()
end

function on_lua_timestep(absTime, dt)
    if Scene and Scene.timestep then Scene:timestep(absTime, dt) end
end

function on_lua_keypressed(key, scancode, action, mods)
    local PRESS = 1
    local REPEAT = 2
    if action == PRESS or action == REPEAT then
        if Scene.keypressed then
            local consumed = Scene:keypressed(key, scancode, action, mods)
            if consumed then return end
        end
    end
    return false
end

function on_lua_charkeypressed(codepoint)
    if Scene.charkeypressed then
        Scene:charkeypressed(string.char(codepoint))
    end
end

function on_lua_settracking(absTime, controllerstate)
    if Scene.settracking then Scene:settracking(absTime, controllerstate) end
end
