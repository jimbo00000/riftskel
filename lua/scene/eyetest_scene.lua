-- eyetest_scene.lua
eyetest_scene = {}

local glfont = nil
require("util.glfont")
local mm = require("util.matrixmath")
local dataDir = nil
local lines = {}

function eyetest_scene.setDataDirectory(dir)
    dataDir = dir
end

function eyetest_scene.initGL()
    for i=1,32 do
        local str = ''
        local a = i + 5
        local n = (a/8)*math.pow((1/.85), a)
        for j=1,n do
            local c = 65+j-1
            local c = 25*math.random() + 65
            str = str..string.char(c)
        end
        table.insert(lines, str)
    end

    local dir = "fonts"
    if dataDir then dir = dataDir .. "/" .. dir end
    glfont = GLFont.new('courier_512.fnt', 'courier_512_0.raw')
    glfont:setDataDirectory(dir)
    glfont:initGL()
end

function eyetest_scene.exitGL()
    glfont:exitGL()
end

function eyetest_scene.render_for_one_eye(view, proj)
    local m = {1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1}
    local s = .01
    mm.glh_translate(m, 0, 2, 0)
    mm.glh_scale(m, s, -s, s)
    mm.pre_multiply(m, view)

    local col = {1, 1, 1}
    local lh = 90
    for k,v in pairs(lines) do
        local wid = glfont:get_string_width(v)
        local xt = 10 - wid/2
        mm.glh_translate(m, xt, 0, 0)
        glfont:render_string(m, proj, col, v)
        mm.glh_translate(m, -xt, 0, 0)
        mm.glh_translate(m, 0, lh, 0)
        local x = .8
        mm.glh_scale(m, x, x, x)
    end
end

function eyetest_scene.timestep(absTime, dt)
end

return eyetest_scene
