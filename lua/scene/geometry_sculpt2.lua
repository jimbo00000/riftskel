-- geometry_sculpt2.lua
-- Uses instanced rendering

scene = {}

local openGL = require("opengl")
local ffi = require("ffi")
local bit = require("bit")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")
local origin = require("scene.origin")

local origin_matrix = {1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1}

-- http://stackoverflow.com/questions/17877224/how-to-prevent-a-lua-script-from-failing-when-a-require-fails-to-find-the-scri
local function prequire(m) 
  local ok, err = pcall(require, m) 
  if not ok then return nil, err end
  return err
end

-- TODO: set package.path to get this dll from somewhere in the project dir tree
--package.path = package.path..'../?.dll;'
local bass = prequire("bass")
if bass == nil then
    print("Could not load Bass library.")
end

local glIntv     = ffi.typeof('GLint[?]')
local glUintv    = ffi.typeof('GLuint[?]')
local glFloatv   = ffi.typeof('GLfloat[?]')

local vbos = {}
local vao = 0
local progs = {}
local instances = {}
local num_insts = 0

local cursorScale = 1

local basic_vert = [[
#version 330

in vec4 vPosition;
in vec4 vColor;

out vec3 vfColor;

uniform mat4 mvmtx;
uniform mat4 prmtx;

void main()
{
    vfColor = vColor.xyz;
    gl_Position = prmtx * mvmtx * vPosition;
}
]]

local basic_frag = [[
#version 330

in vec3 vfColor;
out vec4 fragColor;

void main()
{
    fragColor = vec4(vfColor, .5);
}
]]

--[[
    Drawing instanced cubes: simple color per-vertex.
]]
local per_vertex_color_vert = [[
#version 330
#line 76

in vec4 vPosition;
in vec4 vColor;
in vec4 instancePosition;
in vec4 instanceOrientation;

out vec3 vfColor;

uniform mat4 mvmtx;
uniform mat4 prmtx;

// Quaternion rotation of a vector
vec3 qtransform(vec4 q, vec3 v)
{
    return v + 2.0*cross(cross(v, q.xyz) + q.w*v, q.xyz);
}

void main()
{
    vfColor = vColor.xyz;
    vec3 pos = vPosition.xyz;
    vec4 q = normalize(instanceOrientation);
    pos = qtransform(q, pos);
    float sz = instancePosition.w;
    pos *= sz;
    pos += instancePosition.xyz;
    gl_Position = prmtx * mvmtx * vec4(pos, 1.);
}
]]

local per_vertex_color_frag = [[
#version 330

in vec3 vfColor;
out vec4 fragColor;

void main()
{
    fragColor = vec4(vfColor, 1.0);
}
]]

local function init_cube_attributes()
    local v = {
        0,0,0,
        .1,0,0,
        .1,.1,0,
        0,.1,0,
        0,0,.1,
        .1,0,.1,
        .1,.1,.1,
        0,.1,.1
    }
    for i=1,#v do
        v[i] = v[i] - .05
    end
    local verts = glFloatv(#v, v)

    local c = {
        0,0,0,
        1,0,0,
        1,1,0,
        0,1,0,
        0,0,1,
        1,0,1,
        1,1,1,
        0,1,1
    }
    local cols = glFloatv(#c, c)

    local prog = progs.color
    local vpos_loc = gl.glGetAttribLocation(prog, "vPosition")
    local vcol_loc = gl.glGetAttribLocation(prog, "vColor")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(vbos, vvbo)

    local cvbo = glIntv(0)
    gl.glGenBuffers(1, cvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, cvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(cols), cols, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vcol_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(vbos, cvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vcol_loc)

    local quads = glUintv(6*6, {
        0,3,2, 1,0,2,
        4,5,6, 7,4,6,
        1,2,6, 5,1,6,
        2,3,7, 6,2,7,
        3,0,4, 7,3,4,
        0,1,5, 4,0,5
    })
    local qvbo = glIntv(0)
    gl.glGenBuffers(1, qvbo)
    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, qvbo[0])
    gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, ffi.sizeof(quads), quads, GL.GL_STATIC_DRAW)
    table.insert(vbos, qvbo)
end

local function init_per_instance_attributes()
    local prog = progs.color
    local numPoints = 1024 * 1024
    local sz = 4 * numPoints * ffi.sizeof('GLfloat') -- xyzw

    local insp_loc = gl.glGetAttribLocation(prog, "instancePosition")
    local ipvbo = glIntv(0)
    gl.glGenBuffers(1, ipvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, ipvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_COPY)
    gl.glVertexAttribPointer(insp_loc, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    vbos.inst_positions = ipvbo
    gl.glVertexAttribDivisor(insp_loc, 1)
    gl.glEnableVertexAttribArray(insp_loc)

    local inso_loc = gl.glGetAttribLocation(prog, "instanceOrientation")
    local iovbo = glIntv(0)
    gl.glGenBuffers(1, iovbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, iovbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(inso_loc, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    vbos.inst_orientations = iovbo
    gl.glVertexAttribDivisor(inso_loc, 1)
    gl.glEnableVertexAttribArray(inso_loc)
end

function scene.initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    vao = vaoId[0]
    gl.glBindVertexArray(vao)

    progs.basic = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })
    progs.color = sf.make_shader_from_source({
        vsrc = per_vertex_color_vert,
        fsrc = per_vertex_color_frag,
        })

    init_cube_attributes()
    init_per_instance_attributes()
    gl.glBindVertexArray(0)

    -- Initialize audio library - BASS
    if bass then
        local init_ret = bass.BASS_Init(-1, 44100, 0, 0, nil)
        -- TODO: relative path to keep sounds
        sample = bass.BASS_SampleLoad(false, "../lua/Blip5.wav", 0, 0, 16, 0)
        bass.BASS_Start()
        channel = bass.BASS_SampleGetChannel(sample, false)
    end

    origin.initGL()
end

function scene.exitGL()
    gl.glBindVertexArray(vao)
    for _,v in pairs(vbos) do
        gl.glDeleteBuffers(1,v)
    end
    vbos = {}

    for k,v in pairs(progs) do
        if v then gl.glDeleteProgram(v) end
    end
    local vaoId = ffi.new("GLuint[1]", vao)
    gl.glDeleteVertexArrays(1, vaoId)

    origin.exitGL()
end

-- Draw tracking origin marker over controller location
local function draw_cursor(mview, proj, objmtx)
    origin.render_for_one_eye(mview, proj)
    local mx = {}
    for i=1,16 do mx[i] = mview[i] end
    if origin_matrix[1] ~= nil then
        mm.post_multiply(mx, objmtx)
    end

    local s = cursorScale
    mm.glh_scale(mx, s, s, s)

    -- Draw a translucent cube where it would be placed
    gl.glEnable(GL.GL_BLEND)
    gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA)

    local prog = progs.basic
    local function uni(name) return gl.glGetUniformLocation(prog, name) end
    gl.glUseProgram(prog)
    gl.glUniformMatrix4fv(uni("mvmtx"), 1, GL.GL_FALSE, glFloatv(16, mx))
    gl.glUniformMatrix4fv(uni("prmtx"), 1, GL.GL_FALSE, glFloatv(16, proj))
    gl.glBindVertexArray(vao)
    gl.glDrawElements(GL.GL_TRIANGLES, 6*3*2, GL.GL_UNSIGNED_INT, nil)
    gl.glBindVertexArray(0)

    gl.glDisable(GL.GL_BLEND)
end

function scene.render_for_one_eye(mview, proj)
    local prog = progs.color
    local umv_loc = gl.glGetUniformLocation(prog, "mvmtx")
    local upr_loc = gl.glGetUniformLocation(prog, "prmtx")
    gl.glUseProgram(prog)
    gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, mview))
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))
    
    -- draw geometry items
    gl.glBindVertexArray(vao)
    gl.glDrawElementsInstanced(GL.GL_TRIANGLES, 6*3*2, GL.GL_UNSIGNED_INT, nil, num_insts)
    gl.glBindVertexArray(0)

    draw_cursor(mview, proj, origin_matrix)

    gl.glUseProgram(0)
end

function scene.timestep(absTime, dt)
end

function scene.shoot(mtx)
    local x,y,z = mtx[13], mtx[14], mtx[15]
    local ins = glFloatv(4, {x,y,z,cursorScale})
    local sz = ffi.sizeof(ins)

    local iv = vbos.inst_positions
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, iv[0])
    gl.glBufferSubData(GL.GL_ARRAY_BUFFER, num_insts * sz, sz, ins)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)

    local q = mm.quat_to_matrix(mtx)
    local quat = glFloatv(4, q)
    local io = vbos.inst_orientations
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, io[0])
    gl.glBufferSubData(GL.GL_ARRAY_BUFFER, num_insts * sz, sz, quat)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)

    num_insts = num_insts + 1
end

function scene.keypressed(key)
    if bass then
        bass.BASS_ChannelPlay(channel, false)
    end

    if key == 80 then -- 'p' key
        --[[
        Connect to a running debugger server in ZeroBrane Studio.
          - Choose Project->Start Debugger Server
          - Include mobdebug.lua in lua/ next to scenebridge.lua
          - Include socket/core.dll in the working directory of the app
             TODO: set package.path to get this from within the source tree
          TODO: Can only trigger bp once per reload of lua state.
        ]]
        if (ffi.os == "Windows") then
            --TODO: how do I link to socket package on Linux?
            package.loadlib("socket/core.dll", "luaopen_socket_core")
            local socket = require("socket.core")
        end
        require('mobdebug').start()
    end
end

-- Cast the array cdata ptr(passes from glm::value_ptr(glm::mat4),
-- which gives a float[16]) to a table for further manipulation here in Lua.
function array_to_table2(array)
    local m0 = ffi.cast("float*", array)
    -- The cdata array is 0-indexed. Here we clumsily jam it back
    -- into a Lua-style, 1-indexed table(array portion).
    local tab = {}
    for i=0,15 do tab[i+1] = m0[i] end
    return tab
end

local last_controllerstate = nil

function scene.settracking(absTime, controllerstate)
    if controllerstate == nil then return end

    --local controllerIdx = 1 -- Hydra in 1 and 2
    local controllerIdx = 3 -- Vive wands come through on 3 and 4
    local c = controllerstate[controllerIdx]
    if c == nil then return end

    local viveanybutton = 0x000000020000FFFF

    if last_controllerstate ~= nil then
        local c_ = last_controllerstate[controllerIdx]
        if c_ ~= nil and c ~= nil then
            local m = array_to_table2(c.mtx)
            for i=1,16 do origin_matrix[i] = m[i] end

            --print(c1.ax0x, c1.ax0y)
            if math.abs(c.ax0y) > 0 then
                -- domain is [-1,1]
                local y01 = .5 + .5*c.ax0y
                y01 = math.pow(y01, 3)
                local s = (.01 + y01) * 1
                mm.glh_scale(m, s, s, s)
                cursorScale = s
            end

            local b3lo = bit.band(c.ulButtonPressedLo, viveanybutton) ~= 0
            local b3lo_ = bit.band(c_.ulButtonPressedLo, viveanybutton) ~= 0
            local b3hi = bit.band(c.ulButtonPressedHi, viveanybutton) ~= 0
            local b3hi_ = bit.band(c_.ulButtonPressedHi, viveanybutton) ~= 0
            local b3 = b3lo or b3hi
            local b3_ = b3lo_ or b3hi

            if b3 and not b3_ then
                scene.shoot(m)
            end

            if b3 then
                scene.shoot(m)
            end
        end
    end

    --[[
    if last_controllerstate ~= nil then
        local c1 = controllerstate[1]
        local c0 = last_controllerstate[1]
        if c0 ~= nil and c1 ~= nil then
            local m = array_to_table2(c1.mtx)
            for i=1,16 do origin_matrix[i] = m[i] end

            local hydrabutton3 = bit.lshift(1,3)
            local b3 = bit.band(c1.buttons, hydrabutton3) ~= 0
            local b3_ = bit.band(c0.buttons, hydrabutton3) ~= 0
            if b3 and not b3_ then
                scene.shoot(m)
            end

            local hydrabutton4 = bit.lshift(1,4)
            local b4 = bit.band(c1.buttons, hydrabutton4) ~= 0
            if b4 then
                scene.shoot(m)
            end

        end
    end
    ]]

    last_controllerstate = controllerstate
end

return scene
