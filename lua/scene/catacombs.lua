-- catacombs.lua
catacombs = {}

local openGL = require("opengl")
local ffi = require("ffi")
local sf = require("util.shaderfunctions")

local glIntv = ffi.typeof('GLint[?]')
local glUintv = ffi.typeof('GLuint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')

local vao = 0
local rwwtt_prog = 0
local vbos = {}

local globalTime = 0

local rm_vert = [[
#version 330

in vec4 vPosition;
out vec2 vfUV;

void main()
{
    vfUV = vPosition.xy;
    gl_Position = vPosition;
}
]]

local rm_header = [[
#version 330

in vec2 vfUV;
out vec4 fragColor;

// ShaderToy Inputs:
uniform float iGlobalTime;
uniform sampler2D iChannel0;

uniform mat4 mvmtx;
uniform mat4 prmtx;
]]

local rm_map = [[
float distToBox( in vec3 p, in vec3 abc )
{
    vec3 di = max(abs(p)-abc,0.0);
    return dot(di,di);
}

vec2 column( in float x, in float y, in float z )
{
    vec3 p = vec3( x, y, z );

    float y2=y-0.40;
    float y3=y-0.35;
    float y4=y-1.00;

    float di1=distToBox( p, vec3(0.10*0.85,1.00,0.10*0.85) );
    float di2=distToBox( p, vec3(0.12,0.40,0.12) );
    float di3=distToBox( vec3(x,y4,z), vec3(0.14,0.02,0.14) );
    x = max( abs(p.x), abs(p.z) );
    z = min( abs(p.x), abs(p.z) );  
    float di4=distToBox( vec3(x, y, z), vec3(0.14,0.35,0.05) );
    float di5=distToBox( vec3(x, (y2+z)*0.7071, (z-y2)*0.7071), vec3(0.12, 0.10*0.7071, 0.10*0.7071) );
    float di6=distToBox( vec3(x, (y3+z)*0.7071, (z-y3)*0.7071), vec3(0.14, 0.10*0.7071, 0.10*0.7071) );

    float dm = min(min(min(di5,di6),min(di3,di4)),di2);
    
    vec2 res = vec2( dm, 3.0 );
    if( di1<res.x ) res = vec2( di1, 2.0 );

    return vec2( sqrt(res.x), res.y );
}

vec3 map( in vec3 pos )
{
    float sid = 0.0;
    float dis;

    // floor
    float mindist = pos.y;

    // ceilin
    float x = fract( pos.x+128.0 ) - 0.5;
    float z = fract( pos.z+128.0 ) - 0.5;
    float y = 1.0 - pos.y;
    dis = -sqrt( y*y + min(x*x,z*z)) + 0.4;
    dis = max( dis, y );
    if( dis<mindist )
    {
        mindist = dis;
        sid = 1.0;
    }

    // columns
    float fxc = fract( pos.x+128.5 ) - 0.5;
    float fzc = fract( pos.z+128.5 ) - 0.5;
    vec2 dis2 = column( fxc, pos.y, fzc );
        
    if( dis2.x<mindist )
    {
        mindist = dis2.x;
        sid = dis2.y;
    }

    float dsp = 1.0*clamp(pos.y,0.0,1.0)*abs(sin(6.0*pos.y)*sin(50.0*pos.x)*sin(4.0*6.2831*pos.z));
    mindist -= dsp*0.03;

    return vec3(mindist,sid,dsp);
}
]]

local rm_frag = [[
// Catacombs
// Created by inigo quilez - iq/2013
// License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
// @var title Catacombs
// @var author iq
// @var license CC BY-NC-SA 3.0
// @var url https://www.shadertoy.com/view/lsf3zr
// @var headSize 0.25
// @var eyePos -0.049367368 1.5405077 -5.4223905
// @var tex0 tex09.jpg


float fbm( vec3 p, vec3 n )
{
    p *= 0.15;

    float x = texture2D( iChannel0, p.yz ).x;
    float y = texture2D( iChannel0, p.zx ).x;
    float z = texture2D( iChannel0, p.xy ).x;

    return x*abs(n.x) + y*abs(n.y) + z*abs(n.z);
}

vec3 calcColor( in vec3 pos, in vec3 nor, in float sid )
{
    vec3 col = vec3( 1.0 );

    float kk = fbm( 4.0*pos, nor );

    if( sid<0.5 )
    {

        vec2 peldxz = fract( 12.0*pos.xz );
        peldxz = 4.0*peldxz*(1.0-peldxz);
        float de = 20.0*length(fwidth(pos.xz));
        float peld = smoothstep( 0.15-de, 0.15+de, min( peldxz.x, peldxz.y ) );
        col = 0.05 + 0.95*vec3(peld);
    }
    else if( sid>0.5 && sid<1.5 )
    {
        float fx = fract( pos.x+128.0 ); 
        float fz = fract( pos.z+128.0 ); 
        
        col = vec3(0.7,0.7,0.7);
        
        float p = 1.0;
        p *= smoothstep( 0.02, 0.03, abs(fx-0.1) );
        p *= smoothstep( 0.02, 0.03, abs(fx-0.9) );
        p *= smoothstep( 0.02, 0.03, abs(fz-0.1) );
        p *= smoothstep( 0.02, 0.03, abs(fz-0.9) );
        col = mix( 0.75*vec3(0.3,0.15,0.15), col, p );
    }
    else if( sid>1.5 && sid<2.5 )
    {
        float l = fract( 12.0*pos.y );
        float peld = smoothstep( 0.1, 0.2, l );
        col = 0.05 + 0.95*vec3(peld);
        
    }
    
    
    col *= 2.0*kk;
    
    return col; 
}


vec3 castRay( in vec3 ro, in vec3 rd, in float precis, in float startf, in float maxd )
{
    float h=precis*2.0;
    vec3 c;
    float t = startf;
    float dsp = 0.0;
    float sid = -1.0;
    for( int i=0; i<50; i++ )
    {
        if( abs(h)<precis||t>maxd ) continue;//break;
        t += h;
        vec3 res = map( ro+rd*t );
        h = res.x;
        sid = res.y;
        dsp = res.z;
    }

    if( t>maxd ) sid=-1.0;
    return vec3( t, sid, dsp );
}


float softshadow( in vec3 ro, in vec3 rd, in float mint, in float maxt, in float k )
{
    float res = 1.0;
    float dt = 0.02;
    float t = mint;
    for( int i=0; i<32; i++ )
    {
        if( t<maxt )
        {
        float h = map( ro + rd*t ).x;
        res = min( res, k*h/t );
        t += max( 0.05, dt );
        }
    }
    return clamp( res, 0.0, 1.0 );

}

vec3 calcNormal( in vec3 pos )
{
    vec3 eps = vec3( 0.001, 0.0, 0.0 );
    vec3 nor = vec3(
        map(pos+eps.xyy).x - map(pos-eps.xyy).x,
        map(pos+eps.yxy).x - map(pos-eps.yxy).x,
        map(pos+eps.yyx).x - map(pos-eps.yyx).x );
    return normalize(nor);
}


vec3 doBumpMap( in vec3 pos, in vec3 nor )
{
    float e = 0.0015;
    float b = 0.01;
    
    float ref = fbm( 48.0*pos, nor );
    vec3 gra = -b*vec3( fbm(48.0*vec3(pos.x+e, pos.y, pos.z),nor)-ref,
                        fbm(48.0*vec3(pos.x, pos.y+e, pos.z),nor)-ref,
                        fbm(48.0*vec3(pos.x, pos.y, pos.z+e),nor)-ref )/e;
    
    vec3 tgrad = gra - nor * dot ( nor , gra );
    return normalize ( nor - tgrad );

    
}

float calcAO( in vec3 pos, in vec3 nor )
{
    float ao = 1.0;
    float totao = 0.0;
    float sca = 15.0;
    for( int aoi=0; aoi<5; aoi++ )
    {
        float hr = 0.01 + 0.015*float(aoi*aoi);
        vec3 aopos =  nor * hr + pos;
        float dd = map( aopos ).x;
        totao += -(dd-hr)*sca;
        sca *= 0.5;
    }
    return 1.0 - clamp( totao, 0.0, 1.0 );
}




vec3 render( in vec3 ro, in vec3 rd )
{ 
    // move lights
    vec3 lpos[7];
    vec4 lcol[7];

    for( int i=0; i<7; i++ )
    {
        float la = 1.0;
        lpos[i].x = 0.5 + 2.2*cos(0.22+0.2*iGlobalTime + 17.0*float(i) );
        lpos[i].y = 0.25;
        lpos[i].z = 1.5 + 2.2*cos(2.24+0.2*iGlobalTime + 13.0*float(i) );

        // make the lights avoid the columns
        vec2 ilpos = floor( lpos[i].xz );
        vec2 flpos = lpos[i].xz - ilpos;
        flpos = flpos - 0.5;
        if( length(flpos)<0.2 ) flpos = 0.2*normalize(flpos);
        lpos[i].xz = ilpos + flpos;
        
        float li = sqrt(0.5 + 0.5*sin(2.0*iGlobalTime+ 23.1*float(i)));

        float h = float(i)/8.0;
        vec3 c = mix( vec3(1.0,0.8,0.6), vec3(1.0,0.3,0.05), 0.5+0.5*sin(40.0*h) );
        lcol[i] = vec4( c, li );
    }

    vec3 col = vec3(0.0);
    vec3 res = castRay(ro,rd,0.001,0.025,20.0);
    float t = res.x;
    if( res.y>-0.5 )
    {
        vec3 pos = ro + t*rd;
        vec3 nor = calcNormal( pos );
             col = calcColor( pos, nor, res.y );

        nor = doBumpMap( pos, nor );

        float ao = calcAO( pos, nor );
        ao *= 0.7 + 0.6*res.z;
        // lighting
        vec3 lin = 0.03*ao*vec3(0.25,0.20,0.20)*(0.5+0.5*nor.y);
        vec3 spe = vec3(0.0);
        for( int i=0; i<7; i++ )
        {
            vec3 lig = lpos[i] - pos;
            float llig = dot( lig, lig);
            float im = inversesqrt( llig );
            lig = lig * im;
            float dif = dot( nor, lig );
            dif = clamp( dif, 0.0, 1.0 );
            float at = 2.0*exp2( -2.3*llig )*lcol[i].w;
            dif *= at;
            float at2 = exp2( -0.35*llig );

            float sh = 0.0;
            if( dif>0.02 ) { sh = softshadow( pos, lig, 0.02, sqrt(llig), 32.0 ); dif *= sh; }

            float dif2 = clamp( dot(nor,normalize(vec3(-lig.x,0.0,-lig.z))), 0.0, 1.0 );
            lin += 0.20*ao*dif2*vec3(0.35,0.20,0.10)*at2;
            lin += 2.50*ao*dif*lcol[i].xyz;
            
            float pp = clamp( dot( reflect(rd,nor), lig ), 0.0, 1.0 );
            spe += ao*lcol[i].xyz*at*sh*(pow(pp,16.0) + 0.5*pow(pp,4.0));
        }
      
        // material
        col = mix( col, vec3(0.1,0.3,0.0), sqrt(max(1.0-ao,0.0))*smoothstep(-0.5,-0.1,nor.y) );
        col = mix( col, vec3(0.1,0.3,0.0), (1.0-smoothstep( 0.0, 0.12, abs(nor.y) - 0.1*(1.0-smoothstep(-0.1,0.3,pos.y)) ))*(1.0-smoothstep(0.5,1.0,pos.y)) );
    
        col = col*lin;
        
        col += 3.0*spe*vec3(1.0,0.6,0.2);
        
    }

    col *= exp( -0.055*t*t );

    // lights
    for( int i=0; i<7; i++ )
    {
        vec3 lv = lpos[i] - ro;
        float ll = length( lv );
        if( ll<t )
        {
            float dle = clamp( dot( rd, lv/ll ), 0.0, 1.0 );
            dle = (1.0-smoothstep( 0.0, 0.2*(0.7+0.3*lcol[i].w), acos(dle)*ll ));
            col += dle*6.0*lcol[i].w*lcol[i].xyz*dle*exp( -0.07*ll*ll );;
        }
    }
    

    return vec3( col );
}

vec3 getSceneColor( in vec3 ro, in vec3 rd, inout float depth )
{
    // Move the default camera location to accomodate our default vec3(0, 1.78, -5.0)
    ro += vec3(0.5, -1.25, 3.8);

    vec3 col = render( ro, rd );
    col = sqrt( col );
    return col;
}


///////////////////////////////////////////////////////////////////////////////
// Patch in the Rift's heading to raymarch shader writing out color and depth.
// http://blog.hvidtfeldts.net/

// Translate the origin to the camera's location in world space.
vec3 getEyePoint(mat4 mvmtx)
{
    vec3 ro = -mvmtx[3].xyz;
    return ro;
}

// Construct the usual eye ray frustum oriented down the negative z axis.
// http://antongerdelan.net/opengl/raycasting.html
vec3 getRayDirection(vec2 uv)
{
    vec4 ray_clip = vec4(uv.x, uv.y, -1., 1.);
    vec4 ray_eye = inverse(prmtx) * ray_clip;
    return normalize(vec3(ray_eye.x, ray_eye.y, -1.));
}

void main()
{
    vec2 uv = vfUV;
    vec3 ro = getEyePoint(mvmtx);
    vec3 rd = getRayDirection(uv);

    ro *= mat3(mvmtx);
    rd *= mat3(mvmtx);

    float depth = 9999.0;
    vec3 col = getSceneColor(ro, rd, depth);
    fragColor = vec4(col, 1.0);
    
    // Write to depth buffer
    vec3 eyeFwd = vec3(0.,0.,-1.) * mat3(mvmtx);
    float eyeHitZ = -depth * dot(rd, eyeFwd);
    float p10 = prmtx[2].z;
    float p11 = prmtx[3].z;
    // A little bit of algebra...
    float ndcDepth = -p10 + -p11 / eyeHitZ;
    float dep = ((gl_DepthRange.diff * ndcDepth) + gl_DepthRange.near + gl_DepthRange.far) / 2.0;

    //gl_FragDepth = dep;
}
]]

function scene.setDataDirectory(dir)
    dataDir = dir
end

local function loadtextures()
    local texfilename = "scene/stone_128x128.raw"
    if dataDir then texfilename = dataDir .. "/" .. texfilename end
    local w,h = 128,128
    local inp = io.open(texfilename, "rb")
    local data = nil
    if inp then
        data = inp:read("*all")
        assert(inp:close())
    end
    local dtxId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, dtxId)
    texID = dtxId[0]
    gl.glBindTexture(GL.GL_TEXTURE_2D, texID)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
    --gl.glTexParameteri( GL.GL_TEXTURE_2D, GL.GL_DEPTH_TEXTURE_MODE, GL.GL_INTENSITY ); --deprecated, out in 3.1
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGB,
                  w, h, 0,
                  GL.GL_RGB, GL.GL_UNSIGNED_BYTE, data)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
end

local function make_quad_vbos(prog)
    vbos = {}

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    table.insert(vbos, vvbo)

    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    local verts = glFloatv(4*2, {
        -1,-1,
        1,-1,
        1,1,
        -1,1,
        })
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)

    local vpos_loc = gl.glGetAttribLocation(prog, "vPosition")
    gl.glVertexAttribPointer(vpos_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vpos_loc)

    local quads = glUintv(3*2, {
        0,1,2,
        0,2,3,
    })
    local qvbo = glIntv(0)
    gl.glGenBuffers(1, qvbo)
    table.insert(vbos, qvbo)

    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, qvbo[0])
    gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, ffi.sizeof(quads), quads, GL.GL_STATIC_DRAW)

    return vbos
end

function catacombs.initGL()
    rwwtt_prog = sf.make_shader_from_source({
        vsrc = rm_vert,
        fsrc = rm_header..rm_map..rm_frag,
        })
    
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    vao = vaoId[0]
    gl.glBindVertexArray(vao)
    vbos = make_quad_vbos(rwwtt_prog)
    gl.glBindVertexArray(0)
    loadtextures()
end

function catacombs.exitGL()
    gl.glBindVertexArray(vao)
    for _,v in pairs(vbos) do
        gl.glDeleteBuffers(1,v)
    end
    vbos = {}
    gl.glDeleteProgram(rwwtt_prog)
    local vaoId = ffi.new("GLuint[1]", vao)
    gl.glDeleteVertexArrays(1, vaoId)
end

function catacombs.render_for_one_eye(mview, proj)
    local prog = rwwtt_prog
    gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL.GL_FILL)
    gl.glUseProgram(rwwtt_prog)

    local ugt_loc = gl.glGetUniformLocation(prog, "iGlobalTime")
    gl.glUniform1f(ugt_loc, globalTime)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, texID)
    local stex_loc = gl.glGetUniformLocation(prog, "iChannel0")
    print(stex_loc)
    gl.glUniform1i(stex_loc, 0)

    local umv_loc = gl.glGetUniformLocation(prog, "mvmtx")
    gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, mview))
    local upr_loc = gl.glGetUniformLocation(prog, "prmtx")
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))

    gl.glBindVertexArray(vao)
    gl.glDrawElements(GL.GL_TRIANGLES, 3*2, GL.GL_UNSIGNED_INT, nil)
    gl.glBindVertexArray(0)
    gl.glUseProgram(0)

    if debug_next_draw_call then
        
        --[[
        Connect to a running debugger server in ZeroBrane Studio.
          - Choose Project->Start Debugger Server
          - Include mobdebug.lua in lua/ next to scenebridge.lua
          - Include socket/core.dll in the working directory of the app
             TODO: set package.path to get this from within the source tree
          TODO: Can only trigger bp once per reload of lua state.
        ]]
        if (ffi.os == "Windows") then
            --TODO: how do I link to socket package on Linux?
            package.loadlib("socket/core.dll", "luaopen_socket_core")
            local socket = require("socket.core")
        end
        require('mobdebug').start()
    end
end

function catacombs.timestep(absTime, dt)
    globalTime = absTime
end

function catacombs.keypressed(key)
    if key == 80 then -- 'p' key
        debug_next_draw_call = true
    end
end
return catacombs
