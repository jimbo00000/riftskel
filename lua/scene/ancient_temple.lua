-- ancient_temple.lua
ancient_temple = {}

local openGL = require("opengl")
local ffi = require("ffi")
local sf = require("util.shaderfunctions")

local glIntv = ffi.typeof('GLint[?]')
local glUintv = ffi.typeof('GLuint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')

local vao = 0
local rwwtt_prog = 0
local vbos = {}

local globalTime = 0

local rm_vert = [[
#version 330

in vec4 vPosition;
out vec2 vfUV;

void main()
{
    vfUV = vPosition.xy;
    gl_Position = vPosition;
}
]]

local rm_frag = [[
// raymarch.frag
#version 330
#line 34

in vec2 vfUV;
out vec4 fragColor;

// ShaderToy Inputs:
uniform float iGlobalTime;

uniform mat4 mvmtx;
uniform mat4 prmtx;

// "Ancient Temple" by Kali

// Made in "messy coder" mode, sorry! :D
// I will clean up and comment later...

// @var title Ancient Temple
// @var author Kali
// @var license CC BY-NC-SA 3.0
// @var url https://www.shadertoy.com/view/XslGWf

// @var headSize 0.008
// @var eyePos -0.0011595160 1.5994246 -5.0102816

const int Iterations=14;
const float detail=.00002;
const float Scale=2.;

vec3 lightdir=normalize(vec3(0.,-0.3,-1.));


float ot=0.;
float det=0.;

float hitfloor=0.;

float de(vec3 pos) {
    hitfloor=0.;
    vec3 p=pos;
    p.xz=abs(.5-mod(pos.xz,1.))+.01;
    float DEfactor=1.;
    ot=1000.;
    for (int i=0; i<Iterations; i++) {
        p = abs(p)-vec3(0.,2.,0.);  
        float r2 = dot(p, p);
        ot = min(ot,abs(length(p)));
        float sc=Scale/clamp(r2,0.4,1.);
        p*=sc; 
        DEfactor*=sc;
        p = p - vec3(0.5,1.,0.5);
    }
    float fl=pos.y-3.013;
    float d=min(fl,length(p)/DEfactor-.0005);
    d=min(d,-pos.y+3.9);
    if (abs(d-fl)<.0001) hitfloor=1.;
    return d;
}



vec3 normal(vec3 p) {
    vec3 e = vec3(0.0,det,0.0);
    
    return normalize(vec3(
            de(p+e.yxx)-de(p-e.yxx),
            de(p+e.xyx)-de(p-e.xyx),
            de(p+e.xxy)-de(p-e.xxy)
            )
        );  
}

float shadow(vec3 pos, vec3 sdir) {
        float totalDist =2.0*det, sh=1.;
        for (int steps=0; steps<30; steps++) {
            if (totalDist<1.) {
                vec3 p = pos - totalDist * sdir;
                float dist = de(p)*1.5;
                if (dist < detail)  sh=0.;
                totalDist += max(0.05,dist);
            }
        }
        return max(0.,sh);  
}

float calcAO( const vec3 pos, const vec3 nor ) {
    float aodet=detail*80.;
    float totao = 0.0;
    float sca = 10.0;
    for( int aoi=0; aoi<5; aoi++ ) {
        float hr = aodet + aodet*float(aoi*aoi);
        vec3 aopos =  nor * hr + pos;
        float dd = de( aopos );
        totao += -(dd-hr)*sca;
        sca *= 0.75;
    }
    return clamp( 1.0 - 5.0*totao, 0.0, 1.0 );
}



float kset(vec3 p) {
    p=abs(.5-fract(p*20.));
    float es, l=es=0.;
    for (int i=0;i<13;i++) {
        float pl=l;
        l=length(p);
        p=abs(p)/dot(p,p)-.5;
        es+=exp(-1./abs(l-pl));
    }
    return es;  
}

vec3 light(in vec3 p, in vec3 dir) {
    float hf=hitfloor;
    vec3 n=normal(p);
    float sh=min(1.,shadow(p, lightdir)+hf);
    //float sh=1.;
    float ao=calcAO(p,n);
    float diff=max(0.,dot(lightdir,-n))*sh*1.3;
    float amb=max(0.2,dot(dir,-n))*.4;
    vec3 r = reflect(lightdir,n);
    float spec=pow(max(0.,dot(dir,-r))*sh,10.)*(.5+ao*.5);
    float k=kset(p)*.18; 
    vec3 col=mix(vec3(k*1.1,k*k*1.3,k*k*k),vec3(k),.45)*2.;
    col=col*ao*(amb*vec3(.9,.85,1.)+diff*vec3(1.,.9,.9))+spec*vec3(1,.9,.5)*.7; 
    return col;
}


vec3 raymarch(in vec3 from, in vec3 dir) 
{
    float t=iGlobalTime;
    vec2 lig=vec2(sin(t*2.)*.6,cos(t)*.25-.25);
    float fog,glow,d=1., totdist=glow=fog=0.;
    vec3 p, col=vec3(0.);
    float ref=0.;
    float steps;
    for (int i=0; i<130; i++) {
        if (d>det && totdist<3.5) {
            p=from+totdist*dir;
            d=de(p);
            det=detail*(1.+totdist*55.);
            totdist+=d; 
            glow+=max(0.,.02-d)*exp(-totdist);
            steps++;
        }
    }
    //glow/=steps;
    float l=pow(max(0.,dot(normalize(-dir),normalize(lightdir))),10.);
    vec3 backg=vec3(.8,.85,1.)*.25*(2.-l)+vec3(1.,.9,.65)*l*.4;
    float hf=hitfloor;
    if (d<det) {
        col=light(p-det*dir*1.5, dir); 
        if (hf>0.5) col*=vec3(1.,.85,.8)*.6;
        col*=min(1.2,.5+totdist*totdist*1.5);
        col = mix(col, backg, 1.0-exp(-1.3*pow(totdist,1.3)));
    } else { 
        col=backg;
    }
    col+=glow*vec3(1.,.9,.8)*.34;
    col+=vec3(1,.8,.6)*pow(l,3.)*.5;
    return col; 
}

vec3 getSceneColor( in vec3 from, in vec3 dir, inout float depth )
{
    vec3 eyeoff = vec3(0.0, 3.21-1.78, 0.0);
    vec3 color=raymarch(from+eyeoff,dir);
    
    //col*=length(clamp((.6-pow(abs(uv2),vec2(3.))),vec2(0.),vec2(1.)));
    color*=vec3(1.,.94,.87);
    color=pow(color,vec3(1.2));
    color=mix(vec3(length(color)),color,.85)*.95;
    
    
    return color;
}


///////////////////////////////////////////////////////////////////////////////
// Patch in the Rift's heading to raymarch shader writing out color and depth.
// http://blog.hvidtfeldts.net/

// Translate the origin to the camera's location in world space.
vec3 getEyePoint(mat4 mvmtx)
{
    vec3 ro = -mvmtx[3].xyz;
    return ro;
}

// Construct the usual eye ray frustum oriented down the negative z axis.
// http://antongerdelan.net/opengl/raycasting.html
vec3 getRayDirection(vec2 uv)
{
    vec4 ray_clip = vec4(uv.x, uv.y, -1., 1.);
    vec4 ray_eye = inverse(prmtx) * ray_clip;
    return normalize(vec3(ray_eye.x, ray_eye.y, -1.));
}

void main()
{
    vec2 uv = vfUV;
    vec3 ro = getEyePoint(mvmtx);
    vec3 rd = getRayDirection(uv);

    ro *= mat3(mvmtx);
    rd *= mat3(mvmtx);

    float depth = 9999.0;
    vec3 col = getSceneColor(ro, rd, depth);
    fragColor = vec4(col, 1.0);
    
    // Write to depth buffer
    vec3 eyeFwd = vec3(0.,0.,-1.) * mat3(mvmtx);
    float eyeHitZ = -depth * dot(rd, eyeFwd);
    float p10 = prmtx[2].z;
    float p11 = prmtx[3].z;
    // A little bit of algebra...
    float ndcDepth = -p10 + -p11 / eyeHitZ;
    float dep = ((gl_DepthRange.diff * ndcDepth) + gl_DepthRange.near + gl_DepthRange.far) / 2.0;

    //gl_FragDepth = dep;
}
]]

local function make_quad_vbos(prog)
    vbos = {}

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    table.insert(vbos, vvbo)

    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    local verts = glFloatv(4*2, {
        -1,-1,
        1,-1,
        1,1,
        -1,1,
        })
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)

    local vpos_loc = gl.glGetAttribLocation(prog, "vPosition")
    gl.glVertexAttribPointer(vpos_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vpos_loc)

    local quads = glUintv(3*2, {
        0,1,2,
        0,2,3,
    })
    local qvbo = glIntv(0)
    gl.glGenBuffers(1, qvbo)
    table.insert(vbos, qvbo)

    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, qvbo[0])
    gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, ffi.sizeof(quads), quads, GL.GL_STATIC_DRAW)

    return vbos
end

function ancient_temple.initGL()
    rwwtt_prog = sf.make_shader_from_source({
        vsrc = rm_vert,
        fsrc = rm_frag,
        })
    
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    vao = vaoId[0]
    gl.glBindVertexArray(vao)
    vbos = make_quad_vbos(rwwtt_prog)
    gl.glBindVertexArray(0)
end

function ancient_temple.exitGL()
    gl.glBindVertexArray(vao)
    for _,v in pairs(vbos) do
        gl.glDeleteBuffers(1,v)
    end
    vbos = {}
    gl.glDeleteProgram(rwwtt_prog)
    local vaoId = ffi.new("GLuint[1]", vao)
    gl.glDeleteVertexArrays(1, vaoId)
end

function ancient_temple.render_for_one_eye(mview, proj)
    local prog = rwwtt_prog
    gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL.GL_FILL)
    gl.glUseProgram(rwwtt_prog)

    local ugt_loc = gl.glGetUniformLocation(prog, "iGlobalTime")
    gl.glUniform1f(ugt_loc, globalTime)

    local umv_loc = gl.glGetUniformLocation(prog, "mvmtx")
    gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, mview))
    local upr_loc = gl.glGetUniformLocation(prog, "prmtx")
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))

    gl.glBindVertexArray(vao)
    gl.glDrawElements(GL.GL_TRIANGLES, 3*2, GL.GL_UNSIGNED_INT, nil)
    gl.glBindVertexArray(0)
    gl.glUseProgram(0)

    if debug_next_draw_call then
        
        --[[
        Connect to a running debugger server in ZeroBrane Studio.
          - Choose Project->Start Debugger Server
          - Include mobdebug.lua in lua/ next to scenebridge.lua
          - Include socket/core.dll in the working directory of the app
             TODO: set package.path to get this from within the source tree
          TODO: Can only trigger bp once per reload of lua state.
        ]]
        if (ffi.os == "Windows") then
            --TODO: how do I link to socket package on Linux?
            package.loadlib("socket/core.dll", "luaopen_socket_core")
            local socket = require("socket.core")
        end
        require('mobdebug').start()
    end
end

function ancient_temple.timestep(absTime, dt)
    globalTime = absTime
end

function ancient_temple.keypressed(key)
    if key == 80 then -- 'p' key
        debug_next_draw_call = true
    end
end
return ancient_temple
