-- nothing.lua

nothing = {}

local openGL = require("opengl")

function nothing.initGL()
end

function nothing.exitGL()
end

function nothing.render_for_one_eye(mview, proj)
	gl.glClear(GL.GL_COLOR_BUFFER_BIT)
end

function nothing.timestep(absTime, dt)
end

return nothing

