-- main_glfw.lua

local ffi = require("ffi")
local glfw = require("lib.glfw")
local openGL = require("opengl")
openGL.loader = glfw.glfw.GetProcAddress
openGL:import()
local DEBUG = false

local Scene = require("scene.hybrid_scene")
local PostFX = nil

local mm = require("util.matrixmath")
local fpstimer = require("util.fpstimer")

local win_w = 1200
local win_h = 1000
local objz = -10
local g_ft = fpstimer.create()
local clickpos = {0,0}
local holding = false
local objrot = {0,0}
local objpan = {0,0}
local chassis = {0,0,0}
local keymove = {0,0,0}
local keystates = {}
for i=0, glfw.GLFW.KEY_LAST do
    keystates[i] = glfw.GLFW.RELEASE
end

function switch_to_scene(name)
    if Scene and Scene.exitGL then
        Scene.exitGL()
    end
    package.loaded[name] = nil
    Scene = nil

    if not Scene then
        Scene = require(name)
        if Scene then
            Scene.initGL()
        end
    end
end

function switch_to_effect(name)
    if PostFX and PostFX.exitGL then
        PostFX.exitGL()
    end

    PostFX = nil
    if name == nil then return end
    package.loaded[name] = nil -- is this right? Shouldn't it be the *old* name?

    if not PostFX then
        PostFX = require(name)
        if PostFX then
            PostFX.initGL(win_w, win_h)
        end
    end
end

function onkey(window,k,code,action,mods)
    keystates[k] = action
    local func_table_ctrl = {
        [glfw.GLFW.KEY_F1] = function (x) switch_to_effect(nil) end,
        [glfw.GLFW.KEY_F2] = function (x) switch_to_effect("effect.onepassblur_effect") end,
    }
    local func_table = {
        [glfw.GLFW.KEY_ESCAPE] = function (x) glfw.glfw.SetWindowShouldClose(window, 1) end,
        [glfw.GLFW.KEY_F] = function (x) gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL.GL_FILL) end,
        [glfw.GLFW.KEY_L] = function (x) gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL.GL_LINE) end,

        [glfw.GLFW.KEY_SPACE] = function (x)
            if Scene and Scene.shoot then
                Scene.shoot()
            end
        end,
    }
    if action == glfw.GLFW.PRESS or action == glfw.GLFW.REPEAT then
        if mods == glfw.GLFW.MOD_CONTROL then
            local f = func_table_ctrl[k]
            if f then f() end
        else
            local f = func_table[k]
            if f then f() end
        end
    end

    local mag = 1
    local spd = 10
    local km = {0,0,0}
    if keystates[glfw.GLFW.KEY_D] ~= glfw.GLFW.RELEASE then km[1] = km[1] - spd end
    if keystates[glfw.GLFW.KEY_A] ~= glfw.GLFW.RELEASE then km[1] = km[1] + spd end
    if keystates[glfw.GLFW.KEY_W] ~= glfw.GLFW.RELEASE then km[3] = km[3] + spd end
    if keystates[glfw.GLFW.KEY_S] ~= glfw.GLFW.RELEASE then km[3] = km[3] - spd end
    if keystates[glfw.GLFW.KEY_E] ~= glfw.GLFW.RELEASE then km[2] = km[2] - spd end
    if keystates[glfw.GLFW.KEY_Q] ~= glfw.GLFW.RELEASE then km[2] = km[2] + spd end

    if keystates[glfw.GLFW.KEY_LEFT_CONTROL] ~= glfw.GLFW.RELEASE then mag = 10 * mag end
    if keystates[glfw.GLFW.KEY_LEFT_SHIFT] ~= glfw.GLFW.RELEASE then mag = .1 * mag end
    for i=1,3 do
        keymove[i] = km[i] * mag
    end
end

-- Passing raw character values to scene lets us get away with not
-- including glfw key enums in scenes.
function onchar(window,ch)
    if Scene.key_pressed then
        Scene.key_pressed(string.char(ch))
    end
end

function onclick(window, button, action, mods)
    if action == glfw.GLFW.PRESS then
        holding = button
        local double_buffer = ffi.new("double[2]")
        glfw.glfw.GetCursorPos(window, double_buffer, double_buffer+1)
        local x,y = double_buffer[0], double_buffer[1]
        clickpos = {x,y}
    elseif action == glfw.GLFW.RELEASE then
        holding = nil
    end
end

function onmousemove(window, x, y)
    if holding == glfw.GLFW.MOUSE_BUTTON_1 then
        objrot = {x-clickpos[1], y-clickpos[2]}
        if Scene.onmouse ~= nil then
            Scene.onmouse(x/win_w, y/win_h)
        end
    elseif holding == glfw.GLFW.MOUSE_BUTTON_2 then
        objpan = {.01*(x-clickpos[1]), -.01*(y-clickpos[2])}
    end
end

function onwheel(window,x,y)
    objz = objz + y
    function clamp(x,a,b) return math.min(math.max(x,a),b) end
    objz = clamp(objz,-20,-1)
end

function resize(window, w, h)
    win_w, win_h = w, h
    if PostFx then PostFx.resize_fbo(w,h) end
end

function initGL()
    if Scene then Scene.initGL() end
    if PostFX then PostFX.initGL(win_w, win_h) end
end

function display()
    gl.glViewport(0,0, win_w, win_h)
    gl.glClearColor(0.5, 0.5, 1.0, 0.0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)
    gl.glEnable(GL.GL_DEPTH_TEST)

    if Scene then
        local m = {}
        --mm.glh_lookat(m, {0,2,3}, {0,1.7,2}, {0,1,0})
        mm.make_identity_matrix(m)
        mm.glh_translate(m, chassis[1], chassis[2], chassis[3])
        mm.glh_translate(m, objpan[1], objpan[2], objz)
        mm.glh_rotate(m, objrot[2], 1,0,0)
        mm.glh_rotate(m, objrot[1], 0,1,0)
        local p = {}
        local aspect = win_w / win_h
        mm.glh_perspective_rh(p, 90.0, aspect, .1, 10000)
        Scene.render_for_one_eye(m,p)
        if Scene.set_origin_matrix then Scene.set_origin_matrix(m) end
    end
end

function display_with_effect()
    if PostFX then PostFX.bind_fbo() end
    display()
    if PostFX then PostFX.unbind_fbo() end

    -- Apply post-processing and present
    if PostFX then
        gl.glDisable(GL.GL_DEPTH_TEST)
        gl.glViewport(0,0, win_w, win_h)
        PostFX.present(win_w, win_h)
    end
end

function timestep(absTime, dt)
    if Scene then Scene.timestep(absTime, dt) end
    for i=1,3 do
        chassis[i] = chassis[i] + dt * keymove[i]
    end
end

ffi.cdef[[
typedef unsigned int GLenum;
typedef unsigned int GLuint;
typedef int GLsizei;
typedef char GLchar;
///@todo APIENTRY
typedef void (__stdcall *GLDEBUGPROC)(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam);
]]

local count = 0
local myCallback = ffi.cast("GLDEBUGPROC", function(source, type, id, severity, length, message, userParam)
    if severity == GL.GL_DEBUG_SEVERITY_NOTIFICATION then return end
    local enum_table = {
        [tonumber(GL.GL_DEBUG_SEVERITY_HIGH)] = "SEVERITY_HIGH",
        [tonumber(GL.GL_DEBUG_SEVERITY_MEDIUM)] = "SEVERITY_MEDIUM",
        [tonumber(GL.GL_DEBUG_SEVERITY_LOW)] = "SEVERITY_LOW",
        [tonumber(GL.GL_DEBUG_SEVERITY_NOTIFICATION)] = "SEVERITY_NOTIFICATION",
        [tonumber(GL.GL_DEBUG_SOURCE_API)] = "SOURCE_API",
        [tonumber(GL.GL_DEBUG_SOURCE_WINDOW_SYSTEM)] = "SOURCE_WINDOW_SYSTEM",
        [tonumber(GL.GL_DEBUG_SOURCE_SHADER_COMPILER)] = "SOURCE_SHADER_COMPILER",
        [tonumber(GL.GL_DEBUG_SOURCE_THIRD_PARTY)] = "SOURCE_THIRD_PARTY",
        [tonumber(GL.GL_DEBUG_SOURCE_APPLICATION)] = "SOURCE_APPLICATION",
        [tonumber(GL.GL_DEBUG_SOURCE_OTHER)] = "SOURCE_OTHER",
        [tonumber(GL.GL_DEBUG_TYPE_ERROR)] = "TYPE_ERROR",
        [tonumber(GL.GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR)] = "TYPE_DEPRECATED_BEHAVIOR",
        [tonumber(GL.GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR)] = "TYPE_UNDEFINED_BEHAVIOR",
        [tonumber(GL.GL_DEBUG_TYPE_PORTABILITY)] = "TYPE_PORTABILITY",
        [tonumber(GL.GL_DEBUG_TYPE_PERFORMANCE)] = "TYPE_PERFORMANCE",
        [tonumber(GL.GL_DEBUG_TYPE_MARKER)] = "TYPE_MARKER",
        [tonumber(GL.GL_DEBUG_TYPE_PUSH_GROUP)] = "TYPE_PUSH_GROUP",
        [tonumber(GL.GL_DEBUG_TYPE_POP_GROUP)] = "TYPE_POP_GROUP",
        [tonumber(GL.GL_DEBUG_TYPE_OTHER)] = "TYPE_OTHER",
    }
    print(enum_table[source], enum_table[type], enum_table[severity], id)
    print("   "..ffi.string(message))
    print("   Stack Traceback\n   ===============")
    -- Chop off the first lines of the traceback, as it's always this function.
    local tb = debug.traceback()
    local i = string.find(tb, '\n')
    local i = string.find(tb, '\n', i+1)
    print(string.sub(tb,i+1,-1))
end)

function main()
    for k,v in pairs(arg) do
        if k > 0 then
            if v == '-d' then DEBUG = true end
        end
    end

    glfw.glfw.Init()

    glfw.glfw.WindowHint(glfw.GLFW.DEPTH_BITS, 16)
    glfw.glfw.WindowHint(glfw.GLFW.CONTEXT_VERSION_MAJOR, 4)
    glfw.glfw.WindowHint(glfw.GLFW.CONTEXT_VERSION_MINOR, 1) -- 3 causes MacOSX to segfault
    glfw.glfw.WindowHint(glfw.GLFW.OPENGL_FORWARD_COMPAT, 1)
    glfw.glfw.WindowHint(glfw.GLFW.OPENGL_PROFILE, glfw.GLFW.OPENGL_CORE_PROFILE)
    if DEBUG then
        glfw.glfw.WindowHint(glfw.GLFW.OPENGL_DEBUG_CONTEXT, GL.GL_TRUE)
    end

    window = glfw.glfw.CreateWindow(win_w,win_h,"Luajit",nil,nil)
    glfw.glfw.SetKeyCallback(window, onkey)
    glfw.glfw.SetCharCallback(window, onchar);
    glfw.glfw.SetMouseButtonCallback(window, onclick)
    glfw.glfw.SetCursorPosCallback(window, onmousemove)
    glfw.glfw.SetScrollCallback(window, onwheel)
    glfw.glfw.SetWindowSizeCallback(window, resize)
    glfw.glfw.MakeContextCurrent(window)
    glfw.glfw.SwapInterval(0)

    if DEBUG then
        gl.glDebugMessageCallback(myCallback, nil)
        gl.glDebugMessageControl(GL.GL_DONT_CARE, GL.GL_DONT_CARE, GL.GL_DONT_CARE, 0, nil, GL.GL_TRUE)
        gl.glDebugMessageInsert(GL.GL_DEBUG_SOURCE_APPLICATION, GL.GL_DEBUG_TYPE_MARKER, 0,
            GL.GL_DEBUG_SEVERITY_NOTIFICATION, -1 , "Start debugging")
        gl.glEnable(GL.GL_DEBUG_OUTPUT_SYNCHRONOUS);
    end

    local windowTitle = "OpenGL with Luajit"
    local lastFrameTime = 0
    initGL()
    while glfw.glfw.WindowShouldClose(window) == 0 do
        glfw.glfw.PollEvents()
        g_ft:onFrame()
        display_with_effect()

        local now = os.clock()
        timestep(now, now - lastFrameTime)
        lastFrameTime = now

        if (ffi.os == "Windows") then
            glfw.glfw.SetWindowTitle(window, windowTitle.." "..math.floor(g_ft:getFPS()).." fps")
        end

        glfw.glfw.SwapBuffers(window)
    end
end

main()
