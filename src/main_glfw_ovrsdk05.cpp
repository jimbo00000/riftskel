// main_glfw_ovrsdk06.cpp
// With humongous thanks to cThrough 2014 (Daniel Dekkers)
// Get a window created with GL context and OVR backend initialized,
// then hand off display to the Scene class.

#include <GL/glew.h>
#if defined(_WIN32)
#  include <Windows.h>
#  define GLFW_EXPOSE_NATIVE_WIN32
#  define GLFW_EXPOSE_NATIVE_WGL
#elif defined(__linux__)
#  include <X11/X.h>
#  include <X11/extensions/Xrandr.h>
#  define GLFW_EXPOSE_NATIVE_X11
#  define GLFW_EXPOSE_NATIVE_GLX
#endif
#include <GLFW/glfw3.h>

#if defined(_WIN32) || defined(_LINUX)
#  include <GLFW/glfw3native.h>
#endif

#include <OVR.h>
#include <Kernel/OVR_Types.h> // Pull in OVR_OS_* defines 
#include <OVR_CAPI.h>
#include <OVR_CAPI_GL.h>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <stdio.h>
#include <string.h>
#include <vector>
#include "FBO.h"
#include "Timer.h"
#include "Logger.h"
#include "Scene.h"
#ifdef USE_LUAJIT
#  include "LuajitScene.h"
#endif
#ifdef USE_ANTTWEAKBAR
#  include <AntTweakBar.h>
#endif
#ifdef USE_SIXENSE
#  include <sixense.h>
#  include <sixense_utils/controller_manager/controller_manager.hpp>
#endif

#include "DirectoryFunctions.h"
#include "MatrixFunctions.h"

Timer g_timer;
double g_lastFrameTime = 0.0;
GLFWwindow* g_pMirrorWindow = NULL;
glm::ivec2 g_mirrorWindowSz(1200, 900);

ovrHmd m_Hmd;
ovrFovPort m_EyeFov[2];
ovrGLConfig m_Cfg;
ovrEyeRenderDesc m_EyeRenderDesc[2];
ovrGLTexture m_EyeTexture[2];
unsigned int m_hmdCaps;
unsigned int m_distortionCaps;
bool m_usingDebugHmd;
bool m_directHmdMode;
FBO m_renderBuffer;

IScene* g_pScene = NULL;
std::vector<std::string> g_sceneNames;
int g_currentScene = 0;

#ifdef USE_ANTTWEAKBAR
TwBar* g_pTweakbar = NULL;
#endif

float m_fboScale = 1.f;
float m_cinemaScope = 0.f;

int m_keyStates[GLFW_KEY_LAST];
glm::vec3 m_keyboardMove(0.f);
float m_keyboardYaw = 0.f;
glm::vec3 m_chassisPos(0.f);
float m_chassisYaw = 0.f;

bool swapBackBufferDims = true;

void reinitScene()
{
    if (g_pScene != NULL)
    {
        g_pScene->exitGL();
        delete g_pScene;
    }
#ifdef USE_LUAJIT
    g_pScene = new LuajitScene();
#else
    g_pScene = new Scene();
#endif
    if (g_pScene != NULL)
    {
        g_pScene->initGL();
    }
}

void initAnt()
{
#ifdef USE_ANTTWEAKBAR
    ///@note Bad size errors will be thrown if this is not called before bar creation.
    TwWindowSize(g_mirrorWindowSz.x, g_mirrorWindowSz.y);

    // Create a tweak bar
    g_pTweakbar = TwNewBar("TweakBar");

    TwDefine(" GLOBAL fontsize=3 ");
    TwDefine(" TweakBar size='300 520' ");

    TwAddVarRW(g_pTweakbar, "FBO Scale", TW_TYPE_FLOAT, &m_fboScale,
        " min=0.05 max=1.0 step=0.005 group='Performance' ");
    TwAddVarRW(g_pTweakbar, "Cinemascope", TW_TYPE_FLOAT, &m_cinemaScope,
        " min=0.05 max=1.0 step=0.005 group='Performance' ");
#endif
}

void PopulateSceneList()
{
    const std::string luaPath = "../lua/scene/";
    int i = 0;
    g_sceneNames = GetListOfFilesFromDirectory(luaPath);
}

void NextScene(int delta)
{
    g_currentScene += delta;
    g_currentScene += g_sceneNames.size();
    g_currentScene %= g_sceneNames.size();

    const std::string& sceneName = g_sceneNames[g_currentScene];

    if (g_pScene != NULL)
    {
#ifdef USE_LUAJIT
        LuajitScene* pLJS = dynamic_cast<LuajitScene*>(g_pScene);
        if (pLJS != NULL)
        {
            const int ret = pLJS->SetSceneName(sceneName);
            if (ret != 0)
            {
                // Scene creation failed
                ///@todo maybe remove this entry from the tweakbar?
                LOG_ERROR("LuajitScene::SetSceneName failed with code %d", ret);
                reinitScene();
            }
        }
#endif
    }
}

///@brief Can be called before GL context is initialized.
void initHMD()
{
    ovr_Initialize();

    m_Hmd = ovrHmd_Create(0);
    if (m_Hmd == NULL)
    {
        return;
    }

#ifdef USE_OVR_PERF_LOGGING
    ovrHmd_StartPerfLog(m_Hmd, PROJECT_NAME "-PerfLog.csv", m_logUserData);
#endif

    //const unsigned int caps = ovrHmd_GetEnabledCaps(m_Hmd);
    m_hmdCaps = m_Hmd->HmdCaps;

    if (m_Hmd != NULL)
    {
        const ovrBool ret = ovrHmd_ConfigureTracking(m_Hmd,
            ovrTrackingCap_Orientation | ovrTrackingCap_MagYawCorrection | ovrTrackingCap_Position,
            ovrTrackingCap_Orientation);
        if (ret == 0)
        {
            LOG_ERROR("Error calling ovrHmd_ConfigureTracking.");
        }
    }
}

/// Add together the render target size fields of the HMD laid out side-by-side.
ovrSizei calculateCombinedTextureSize(ovrHmd pHmd)
{
    ovrSizei texSz = {0};
    if (pHmd == NULL)
        return texSz;

    const ovrSizei szL = ovrHmd_GetFovTextureSize(pHmd, ovrEye_Left, pHmd->DefaultEyeFov[ovrEye_Left], 1.f);
    const ovrSizei szR = ovrHmd_GetFovTextureSize(pHmd, ovrEye_Right, pHmd->DefaultEyeFov[ovrEye_Right], 1.f);
    texSz.w = szL.w + szR.w;
    texSz.h = std::max(szL.h, szR.h);
    return texSz;
}

///@brief Called once a GL context has been set up.
void initVR()
{
    if (m_Hmd == NULL)
        return;

    m_Cfg.OGL.Header.BackBufferSize = m_Hmd->Resolution;

    ///@note This operation seems necessary for proper placement of the output window
    /// in Linux's Direct mode (Rift configured as Screen 1).
    if (swapBackBufferDims)
    {
        int tmp = m_Cfg.OGL.Header.BackBufferSize.w;
        m_Cfg.OGL.Header.BackBufferSize.w = m_Cfg.OGL.Header.BackBufferSize.h;
        m_Cfg.OGL.Header.BackBufferSize.h = tmp;
    }

    //ConfigureRendering();


    const ovrSizei texSz = calculateCombinedTextureSize(m_Hmd);
    deallocateFBO(m_renderBuffer);
    allocateFBO(m_renderBuffer, texSz.w, texSz.h);

    ovrGLTexture& texL = m_EyeTexture[ovrEye_Left];
    ovrGLTextureData& texDataL = texL.OGL;
    ovrTextureHeader& hdrL = texDataL.Header;

    hdrL.API = ovrRenderAPI_OpenGL;
    hdrL.TextureSize.w = texSz.w;
    hdrL.TextureSize.h = texSz.h;
    hdrL.RenderViewport.Pos.x = 0;
    hdrL.RenderViewport.Pos.y = 0;
    hdrL.RenderViewport.Size.w = texSz.w / 2;
    hdrL.RenderViewport.Size.h = texSz.h;
    texDataL.TexId = m_renderBuffer.tex;

    // Right eye the same, except for the x-position in the texture.
    ovrGLTexture& texR = m_EyeTexture[ovrEye_Right];
    texR = texL;
    texR.OGL.Header.RenderViewport.Pos.x = (texSz.w + 1) / 2;

    for (int ei=0; ei<ovrEye_Count; ++ei)
    {
        m_EyeFov[ei] = m_Hmd->DefaultEyeFov[ei];
    }


    ///@todo Do we need to choose here?
    //ConfigureSDKRendering();
    m_Cfg.OGL.Header.API = ovrRenderAPI_OpenGL;
    m_Cfg.OGL.Header.Multisample = 0;

    m_distortionCaps =
        ovrDistortionCap_TimeWarp |
        ovrDistortionCap_Vignette;
#ifdef _LINUX
    m_distortionCaps |= ovrDistortionCap_LinuxDevFullscreen;
#endif
    ovrHmd_ConfigureRendering(m_Hmd, &m_Cfg.Config, m_distortionCaps, m_EyeFov, m_EyeRenderDesc);

}

glm::mat4 makeWorldToChassisMatrix()
{
    return makeChassisMatrix_glm(m_chassisYaw, 0.f, 0.f, m_chassisPos);
}

void _resetGLState()
{
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthRangef(0.0f, 1.0f);
    glDepthFunc(GL_LESS);

    glDisable(GL_CULL_FACE);
    glFrontFace(GL_CCW);
}

// Display the old-fashioned way, to a monoscopic viewport on a desktop monitor.
void displayMonitor()
{
    if (g_pScene == NULL)
        return;

    const glm::mat4 mview = makeWorldToChassisMatrix();
    const glm::ivec2 vp = g_mirrorWindowSz;
    const glm::mat4 persp = glm::perspective(
        90.f,
        static_cast<float>(vp.x) / static_cast<float>(vp.y),
        .004f,
        500.f);
    glViewport(0, 0, vp.x, vp.y);
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDisable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    g_pScene->RenderForOneEye(glm::value_ptr(glm::inverse(mview)), glm::value_ptr(persp));
}

// Display to an HMD with OVR SDK backend.
void displayHMD()
{
    if (m_Hmd == NULL)
    {
        displayMonitor();
        return;
    }

    ovrHmd hmd = m_Hmd;

    //const ovrFrameTiming hmdFrameTiming =
    ovrHmd_BeginFrame(m_Hmd, 0);

    bindFBO(m_renderBuffer);

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    ovrVector3f e2v[2] = {
        m_EyeRenderDesc[0].HmdToEyeViewOffset,
        m_EyeRenderDesc[1].HmdToEyeViewOffset,
    };
    ovrTrackingState outHmdTrackingState;
    ovrPosef outEyePoses[2];
    ovrHmd_GetEyePoses(
        hmd,
        0,
        e2v, // could this parameter be const?
        outEyePoses,
        &outHmdTrackingState);

    // For passing to EndFrame once rendering is done
    ovrPosef renderPose[2];
    ovrTexture eyeTexture[2];
    for (int eyeIndex=0; eyeIndex<ovrEye_Count; eyeIndex++)
    {
        const ovrEyeType e = hmd->EyeRenderOrder[eyeIndex];

        const ovrPosef eyePose = outEyePoses[e];
        renderPose[e] = eyePose;
        eyeTexture[e] = m_EyeTexture[e].Texture;

        const ovrGLTexture& otex = m_EyeTexture[e];
        const ovrRecti& rvp = otex.OGL.Header.RenderViewport;
        glViewport(
            static_cast<int>(m_fboScale * rvp.Pos.x),
            static_cast<int>(m_fboScale * rvp.Pos.y),
            static_cast<int>(m_fboScale * rvp.Size.w),
            static_cast<int>(m_fboScale * rvp.Size.h)
            );

        const OVR::Matrix4f proj = ovrMatrix4f_Projection(
            m_EyeRenderDesc[e].Fov, 0.01f, 10000.0f, true);

        const glm::mat4 viewLocal = makeMatrixFromPose(eyePose);
        //const glm::mat4 viewWorld = makeWorldToChassisMatrix() * viewLocal;
        const glm::mat4 viewWorld =
            makeWorldToChassisMatrix() *
            makeMatrixFromPose(eyePose);
        const glm::mat4 prMtx = glm::transpose(glm::make_mat4(&proj.M[0][0]));

        _resetGLState();

        g_pScene->RenderForOneEye(glm::value_ptr(glm::inverse(viewWorld)), glm::value_ptr(prMtx));
    }
    unbindFBO();

    // Inform SDK of downscaled texture target size(performance scaling)
    for (int i=0; i<ovrEye_Count; ++i)
    {
        const ovrSizei& ts = m_EyeTexture[i].Texture.Header.TextureSize;
        ovrRecti& rr = eyeTexture[i].Header.RenderViewport;
        rr.Size.w = static_cast<int>(static_cast<float>(ts.w/2) * m_fboScale);
        rr.Size.h = static_cast<int>(static_cast<float>(ts.h) * m_fboScale);
        rr.Pos.x = i * rr.Size.w;
    }
    ovrHmd_EndFrame(m_Hmd, renderPose, eyeTexture);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glUseProgram(0);

}

void exitVR()
{
#ifdef USE_OVR_PERF_LOGGING
    ovrHmd_StopPerfLog(m_Hmd);
#endif

    deallocateFBO(m_renderBuffer);
    ovrHmd_Destroy(m_Hmd);
    ovr_Shutdown();
}

static void ErrorCallback(int p_Error, const char* p_Description)
{
    (void)p_Error;
    (void)p_Description;
    LOG_INFO("ERROR: %d, %s", p_Error, p_Description);
}

void keyboard(GLFWwindow* pWindow, int key, int codes, int action, int mods)
{
    (void)pWindow;
    (void)codes;

    if ((key > -1) && (key <= GLFW_KEY_LAST))
    {
        m_keyStates[key] = action;
    }

    ovrHmd_DismissHSWDisplay(m_Hmd);

    if (action == GLFW_PRESS)
    {
    switch (key)
    {
        default:
            if (g_pScene != NULL)
            {
                g_pScene->keypressed(key);
            }
            break;

        case GLFW_KEY_W:
        case GLFW_KEY_A:
        case GLFW_KEY_S:
        case GLFW_KEY_D:
            break;

        case GLFW_KEY_SPACE:
            ovrHmd_RecenterPose(m_Hmd);
            break;

        case GLFW_KEY_R:
            m_chassisPos = glm::vec3(0.f);
            break;

        case GLFW_KEY_GRAVE_ACCENT:
        case GLFW_KEY_TAB:
        case GLFW_KEY_ENTER:
            {
                int delta = 1;
                if (mods & GLFW_MOD_SHIFT)
                {
                    delta = -1;
                }
                NextScene(delta);
            }
            break;

        case GLFW_KEY_F5:
            reinitScene();
            break;

        case GLFW_KEY_ESCAPE:
            glfwSetWindowShouldClose(g_pMirrorWindow, 1);
            break;
    }
    }
    else if (action == GLFW_RELEASE)
    {
        switch (key)
        {
        default:
            break;
        }
    }

    // Handle keyboard movement(WASD keys)
    const glm::vec3 forward(0.f, 0.f, -1.f);
    const glm::vec3 up(0.f, 1.f, 0.f);
    const glm::vec3 right(1.f, 0.f, 0.f);
    glm::vec3 keyboardMove(0.f);
    float keyboardYaw = 0.f;
    if (m_keyStates['W'] != GLFW_RELEASE) { keyboardMove += forward; }
    if (m_keyStates['S'] != GLFW_RELEASE) { keyboardMove -= forward; }
    if (m_keyStates['A'] != GLFW_RELEASE) { keyboardMove -= right; }
    if (m_keyStates['D'] != GLFW_RELEASE) { keyboardMove += right; }
    if (m_keyStates['Q'] != GLFW_RELEASE) { keyboardMove -= up; }
    if (m_keyStates['E'] != GLFW_RELEASE) { keyboardMove += up; }
    if (m_keyStates[GLFW_KEY_UP] != GLFW_RELEASE) { keyboardMove += forward; }
    if (m_keyStates[GLFW_KEY_DOWN] != GLFW_RELEASE) { keyboardMove -= forward; }
    if (m_keyStates[GLFW_KEY_LEFT] != GLFW_RELEASE) { keyboardMove -= right; }
    if (m_keyStates[GLFW_KEY_RIGHT] != GLFW_RELEASE) { keyboardMove += right; }
    if (m_keyStates['1'] != GLFW_RELEASE) { keyboardYaw -= 1.f; }
    if (m_keyStates['3'] != GLFW_RELEASE) { keyboardYaw += 1.f; }

    float mag = 1.f;
    if (m_keyStates[GLFW_KEY_LEFT_SHIFT] != GLFW_RELEASE)
        mag *= .1f;
    if (m_keyStates[GLFW_KEY_LEFT_CONTROL] != GLFW_RELEASE)
        mag *= 10.f;
    m_keyboardMove = mag * keyboardMove;
    m_keyboardYaw = mag * keyboardYaw;
}

void mouseDown(GLFWwindow* pWindow, int button, int action, int mods)
{
    (void)pWindow;
    (void)mods;
}

void mouseMove(GLFWwindow* pWindow, double xd, double yd)
{
    glfwGetCursorPos(pWindow, &xd, &yd);
    const int x = static_cast<int>(xd);
    const int y = static_cast<int>(yd);
}

void timestep()
{
    const double absT = g_timer.seconds();
    const double dt = absT - g_lastFrameTime;
    g_lastFrameTime = absT;
    if (g_pScene != NULL)
    {
        g_pScene->timestep(absT, dt);
    }

    // Move in the direction the viewer is facing.
    const glm::vec3 move_dt = m_keyboardMove * static_cast<float>(dt);
    const glm::mat4 moveTxfm = glm::mat4(1.f);//makeMatrixFromPose(m_eyePoses[0]);
    const glm::vec4 mv4 = moveTxfm * glm::vec4(move_dt, 0.f);
    m_chassisPos += glm::vec3(mv4);

    {
        const float rotSpeed = 10.f;
        m_chassisYaw += m_keyboardYaw * static_cast<float>(dt);
    }

    if (g_pScene != NULL)
    {
#ifdef USE_SIXENSE
        const int maxBases = sixenseGetMaxBases();
        for (int base = 0; base < maxBases; ++base)
        {
            sixenseSetActiveBase(base);
            if (!sixenseIsBaseConnected(base))
                continue;
            ///@todo Handle Multiple bases
            sixenseAllControllerData acd;
            sixenseGetAllNewestData(&acd);
            const double absT = g_timer.seconds();
            g_pScene->setTracking_Hydra(absT, (void*)&acd);
        }
#endif // USE_SIXENSE
    }
}

void resize(GLFWwindow* pWindow, int w, int h)
{
    (void)pWindow;
    g_mirrorWindowSz.x = w;
    g_mirrorWindowSz.y = h;
}

// OpenGL debug callback
void GLAPIENTRY myCallback(
    GLenum source, GLenum type, GLuint id, GLenum severity,
    GLsizei length, const GLchar *msg,
    const void *data)
{
    switch (severity)
    {
    case GL_DEBUG_SEVERITY_HIGH:
    case GL_DEBUG_SEVERITY_MEDIUM:
    case GL_DEBUG_SEVERITY_LOW:
        LOG_INFO("[[GL Debug]] %x %x %x %x %s", source, type, id, severity, msg);
        break;
    case GL_DEBUG_SEVERITY_NOTIFICATION:
        break;
    }
}

int main(int argc, char** argv)
{
    LOG_INFO("Compiled against GLFW %i.%i.%i",
        GLFW_VERSION_MAJOR,
        GLFW_VERSION_MINOR,
        GLFW_VERSION_REVISION);
    int major, minor, revision;
    glfwGetVersion(&major, &minor, &revision);
    LOG_INFO("Running against GLFW %i.%i.%i", major, minor, revision);
    LOG_INFO("glfwGetVersionString: %s", glfwGetVersionString());

    initHMD();

    glfwSetErrorCallback(ErrorCallback);
    if (!glfwInit())
    {
        exit(EXIT_FAILURE);
    }
    glfwWindowHint(GLFW_DEPTH_BITS, 16);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#ifdef _DEBUG
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
#endif

    GLFWmonitor* monitor = glfwGetPrimaryMonitor();
    const GLFWvidmode* mode = glfwGetVideoMode(monitor);
    int ww = mode->width;
    int wh = mode->height;
    if (m_Hmd == NULL)
    {
        monitor = NULL;
        ww = 1000;
        wh = 800;
    }
    GLFWwindow* l_Window = glfwCreateWindow(ww, wh, "HMD window", monitor, NULL);
    if (!l_Window)
    {
        LOG_ERROR("Glfw failed to create a window. Exiting.");
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

#ifdef _LINUX
    m_Cfg.OGL.Disp = NULL;
#endif

    glfwMakeContextCurrent(l_Window);
    glfwSetKeyCallback(l_Window, keyboard);
    glfwSetMouseButtonCallback(l_Window, mouseDown);
    glfwSetCursorPosCallback(l_Window, mouseMove);
    glfwSetWindowSizeCallback(l_Window, resize);
    g_pMirrorWindow = l_Window;

    memset(m_keyStates, 0, GLFW_KEY_LAST*sizeof(int));

    glfwMakeContextCurrent(l_Window);

    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK)
    {
        LOG_INFO("glewInit() error.");
        exit(EXIT_FAILURE);
    }

#ifdef _DEBUG
    // Debug callback initialization
    // Must be done *after* glew initialization.
    glDebugMessageCallback(myCallback, NULL);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_TRUE);
    glDebugMessageInsert(GL_DEBUG_SOURCE_APPLICATION, GL_DEBUG_TYPE_MARKER, 0,
        GL_DEBUG_SEVERITY_NOTIFICATION, -1 , "Start debugging");
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
#endif

#ifdef USE_ANTTWEAKBAR
    TwInit(TW_OPENGL, NULL);
    initAnt();
#endif
    PopulateSceneList();

#ifdef USE_LUAJIT
    g_pScene = new LuajitScene();
#else
    g_pScene = new Scene();
#endif
    if (g_pScene != NULL)
        g_pScene->initGL();
    initVR();
#ifdef USE_SIXENSE
    sixenseInit();
#endif

    while (!glfwWindowShouldClose(l_Window))
    {
        glfwPollEvents();
        timestep();

        glClearColor(1.0, 0.0, 0.0, 0.0);
        glClear(GL_COLOR_BUFFER_BIT);
        displayHMD();
        // OVR SDK 05 does its own swap
        if (m_Hmd == NULL)
        {
            glfwSwapBuffers(l_Window);
        }
    }
    exitVR();
    g_pScene->exitGL();
#ifdef USE_SIXENSE
    sixenseExit();
#endif
    glfwDestroyWindow(l_Window);
    glfwTerminate();
    exit(EXIT_SUCCESS);
}
