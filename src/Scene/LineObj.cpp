// LineObj.cpp

#include "LineObj.h"
#include "StringFunctions.h"

#include <fstream>
#include <string>

LineObj::LineObj()
 : m_verts()
 , m_idxs()
 , m_segments()
 , m_ucolor()
{
}

LineObj::~LineObj()
{
}

///@return 0 for success, non-zero otherwise
int LineObj::LoadFromObjFile(const std::string& filename)
{
    std::ifstream file(filename.c_str());
    if (file.is_open() == false)
    {
        return 1;
    }

    int basev = 1;
    std::string line;
    while (std::getline(file, line))
    {
        const std::vector<std::string> toks = split(line, ' ');
        if (toks.size() < 2)
            continue;
        const std::string& t = toks[0];
        if (t.empty())
            continue;

        if (t[0] == 'o')
        {
            lineSegment ls;
            ls.numVerts = m_idxs.size() - basev - 1;
            ls.baseVert = basev;
            if (ls.numVerts > 0)
            {
                m_segments.push_back(ls);
            }
            basev = m_idxs.size();
        }
        else if (t[0] == 'v')
        {
            if (toks.size() < 4)
                continue;
            vf3 v;
            v.x = static_cast<float>(strtod(toks[1].c_str(), NULL));
            v.y = static_cast<float>(strtod(toks[2].c_str(), NULL));
            v.z = static_cast<float>(strtod(toks[3].c_str(), NULL));
            m_verts.push_back(v);
        }
        else if (t[0] == 'l')
        {
            for (std::vector<std::string>::const_iterator it = toks.begin() + 1;
                it != toks.end();
                ++it)
            {
                const std::string t = *it;
                const int f = static_cast<int>(strtol(t.c_str(), NULL, 0));
                m_idxs.push_back(f);
            }
        }
    }

    // Add the last object
    {
        lineSegment ls;
        ls.numVerts = m_idxs.size() - basev - 1;
        ls.baseVert = basev;
        m_segments.push_back(ls);
    }

    return 0;
}

///@brief While the basic VAO is bound, gen and bind all buffers and attribs.
void LineObj::_InitObjAttributes()
{
    GLuint vertVbo = 0;
    glGenBuffers(1, &vertVbo);
    m_ucolor.AddVbo("vPosition", vertVbo);
    glBindBuffer(GL_ARRAY_BUFFER, vertVbo);
    glBufferData(GL_ARRAY_BUFFER, m_verts.size()*sizeof(vf3), &m_verts[0], GL_STATIC_DRAW);
    glVertexAttribPointer(m_ucolor.GetAttrLoc("vPosition"), 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(m_ucolor.GetAttrLoc("vPosition"));

    GLuint quadVbo = 0;
    glGenBuffers(1, &quadVbo);
    m_ucolor.AddVbo("elements", quadVbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, quadVbo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_idxs.size()*sizeof(GLuint), &m_idxs[0], GL_STATIC_DRAW);
}

void LineObj::initGL()
{
    m_ucolor.initProgram("ucolor");
    m_ucolor.bindVAO();
    _InitObjAttributes();
    glBindVertexArray(0);
}

void LineObj::exitGL()
{
    m_ucolor.destroy();
}