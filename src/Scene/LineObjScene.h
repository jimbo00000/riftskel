// LineObjScene.h

#pragma once

#ifdef _WIN32
#  define WINDOWS_LEAN_AND_MEAN
#  define NOMINMAX
#  include <windows.h>
#endif
#include <stdlib.h>
#include <GL/glew.h>

#include <glm/glm.hpp>
#include <vector>

#include "IScene.h"
#include "LineObj.h"
#include "ShaderWithVariables.h"

///@brief 
class LineObjScene : public IScene
{
public:
    LineObjScene();
    virtual ~LineObjScene();

    virtual void initGL();
    virtual void exitGL();
    virtual void timestep(double absTime, double dt);
    virtual void RenderForOneEye(const float* pMview, const float* pPersp) const;

    virtual bool RayIntersects(
        const float* pRayOrigin,
        const float* pRayDirection,
        float* pTParameter, // [inout]
        float* pHitLocation, // [inout]
        float* pHitNormal // [inout]
        ) const { return false; }

protected:
    void _InitPlaneAttributes();

    ShaderWithVariables m_plane;

    std::vector<LineObj*> m_objs;

private: // Disallow copy ctor and assignment operator
    LineObjScene(const LineObjScene&);
    LineObjScene& operator=(const LineObjScene&);
};
