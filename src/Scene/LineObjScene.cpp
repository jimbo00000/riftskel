// Scene.cpp

#include "LineObjScene.h"

#ifdef __APPLE__
#include "opengl/gl.h"
#endif

#ifdef _WIN32
#  define WINDOWS_LEAN_AND_MEAN
#  define NOMINMAX
#  include <windows.h>
#endif

#define _USE_MATH_DEFINES
#include <math.h>

#include <stdlib.h>
#include <string.h>
#include <vector>

#include <GL/glew.h>

#include "Logger.h"

LineObjScene::LineObjScene()
: m_plane()
{
}

LineObjScene::~LineObjScene()
{
    for (std::vector<LineObj*>::iterator it = m_objs.begin();
        it != m_objs.end();
        ++it)
    {
        LineObj* pLO = *it;
        delete pLO;
    }
}

void LineObjScene::initGL()
{
    std::vector<std::string> filenames;

    ///@todo Get filenames from directory
    filenames.push_back("amirahouse10.obj");
    filenames.push_back("armytanks9.obj");

    for (std::vector<std::string>::const_iterator it = filenames.begin();
        it != filenames.end();
        ++it)
    {
        const std::string& f = *it;
        LineObj* pLO = new LineObj();
        if (pLO != NULL)
        {
            pLO->LoadFromObjFile(f);
            m_objs.push_back(pLO);
        }
    }

    for (std::vector<LineObj*>::iterator it = m_objs.begin();
        it != m_objs.end();
        ++it)
    {
        LineObj* pLO = *it;
        if (pLO != NULL)
        {
            pLO->initGL();
        }
    }

    m_plane.initProgram("basicplane");
    m_plane.bindVAO();
    _InitPlaneAttributes();
    glBindVertexArray(0);
}

void LineObjScene::exitGL()
{
    for (std::vector<LineObj*>::iterator it = m_objs.begin();
        it != m_objs.end();
        ++it)
    {
        LineObj* pLO = *it;
        if (pLO != NULL)
        {
            pLO->exitGL();
        }
    }

    m_plane.destroy();
}

///@brief While the basic VAO is bound, gen and bind all buffers and attribs.
void LineObjScene::_InitPlaneAttributes()
{
    const glm::vec3 minPt(-10.0f, 0.0f, -10.0f);
    const glm::vec3 maxPt(10.0f, 0.0f, 10.0f);
    const float verts[] = {
        minPt.x, minPt.y, minPt.z,
        minPt.x, minPt.y, maxPt.z,
        maxPt.x, minPt.y, maxPt.z,
        maxPt.x, minPt.y, minPt.z,
    };
    GLuint vertVbo = 0;
    glGenBuffers(1, &vertVbo);
    m_plane.AddVbo("vPosition", vertVbo);
    glBindBuffer(GL_ARRAY_BUFFER, vertVbo);
    glBufferData(GL_ARRAY_BUFFER, 4*3*sizeof(GLfloat), verts, GL_STATIC_DRAW);
    glVertexAttribPointer(m_plane.GetAttrLoc("vPosition"), 3, GL_FLOAT, GL_FALSE, 0, NULL);

    const float texs[] = {
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,
    };
    GLuint colVbo = 0;
    glGenBuffers(1, &colVbo);
    m_plane.AddVbo("vTexCoord", colVbo);
    glBindBuffer(GL_ARRAY_BUFFER, colVbo);
    glBufferData(GL_ARRAY_BUFFER, 4*2*sizeof(GLfloat), texs, GL_STATIC_DRAW);
    glVertexAttribPointer(m_plane.GetAttrLoc("vTexCoord"), 2, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(m_plane.GetAttrLoc("vPosition"));
    glEnableVertexAttribArray(m_plane.GetAttrLoc("vTexCoord"));

    const unsigned int tris[] = {
        0,3,2, 1,0,2, // ccw
    };
    GLuint triVbo = 0;
    glGenBuffers(1, &triVbo);
    m_plane.AddVbo("elements", triVbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triVbo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 2*3*sizeof(GLuint), tris, GL_STATIC_DRAW);
}

void LineObjScene::RenderForOneEye(const float* pMview, const float* pPersp) const
{
    if (m_bDraw == false)
        return;
    if (m_objs.empty())
        return;

    for (std::vector<LineObj*>::const_iterator it = m_objs.begin();
        it != m_objs.end();
        ++it)
    {
        const LineObj* pLO = *it;
        if (pLO == NULL)
            continue;

        const ShaderWithVariables& swv = pLO->m_ucolor;
        glUseProgram(swv.prog());
        {
            glUniformMatrix4fv(swv.GetUniLoc("mvmtx"), 1, false, pMview);
            glUniformMatrix4fv(swv.GetUniLoc("prmtx"), 1, false, pPersp);

            glUniform3f(swv.GetUniLoc("uColor"), 1.f, 1.f, 1.f);

            swv.bindVAO();
            {
                const std::vector<lineSegment>& segs = pLO->m_segments;
                for (std::vector<lineSegment>::const_iterator it = segs.begin();
                    it != segs.end();
                    ++it)
                {
                    const lineSegment& ls = *it;
                    glDrawElementsBaseVertex(GL_LINE_STRIP,
                        ls.numVerts,
                        GL_UNSIGNED_INT,
                        0,
                        ls.baseVert);
                }
            }
            glBindVertexArray(0);
        }
        glUseProgram(0);
    }
}

void LineObjScene::timestep(double /*absTime*/, double dt)
{
}
