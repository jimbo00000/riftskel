// LineObj.h

#pragma once

#ifdef _WIN32
#  define WINDOWS_LEAN_AND_MEAN
#  define NOMINMAX
#  include <windows.h>
#endif
#include <stdlib.h>
#include <GL/glew.h>

#include <vector>

#include "IScene.h"
#include "ShaderWithVariables.h"

struct vf3 {
    float x, y, z;
};

struct lineSegment {
    int numVerts;
    int baseVert;
};

///@brief Loads obj files containing only line segments and vertices.
class LineObj
{
public:
    LineObj();
    virtual ~LineObj();

    int LoadFromObjFile(const std::string& filename);
    virtual void initGL();
    virtual void exitGL();

//protected:
    void _InitObjAttributes();

    std::vector<vf3> m_verts;
    std::vector<GLuint> m_idxs;
    std::vector<lineSegment> m_segments;
    ShaderWithVariables m_ucolor;

private: // Disallow copy ctor and assignment operator
    LineObj(const LineObj&);
    LineObj& operator=(const LineObj&);
};
