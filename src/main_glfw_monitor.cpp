// main_glfw_monitor.cpp
// Regular old monitor display.

#include <GL/glew.h>
#if defined(_WIN32)
#  include <Windows.h>
#endif
#include <GLFW/glfw3.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <stdio.h>
#include <string.h>
#include <vector>
#include "FBO.h"
#include "Timer.h"
#include "Logger.h"
#include "Scene.h"
#ifdef USE_LUAJIT
#include "LuajitScene.h"
#endif
#ifdef USE_ANTTWEAKBAR
#  include <AntTweakBar.h>
#endif

#ifdef USE_SIXENSE
#include <sixense.h>
#include <sixense_utils/controller_manager/controller_manager.hpp>
#endif // USE_SIXENSE

#include "DirectoryFunctions.h"
#include "MatrixFunctions.h"

Timer g_timer;
double g_lastFrameTime = 0.0;
GLFWwindow* g_pMirrorWindow = NULL;
glm::ivec2 g_mirrorWindowSz(1200, 900);

IScene* g_pScene = NULL;

#ifdef USE_ANTTWEAKBAR
TwBar* g_pTweakbar = NULL;
TwBar* g_pShaderTweakbar = NULL;
#endif

float m_fboScale = 1.f;
float m_cinemaScope = 0.f;

int m_keyStates[GLFW_KEY_LAST];
glm::vec3 m_keyboardMove(0.f);
float m_keyboardYaw = 0.f;
glm::vec3 m_chassisPos(0.f);
float m_chassisYaw = 0.f;

int g_sceneIdx = 0;

///@brief Get a list of available Lua scene filenames.
std::vector<std::string> getLuaSceneNames()
{
    const std::string luaPath = "../lua/scene2/";
    std::vector<std::string> filenames = GetListOfFilesFromDirectory(luaPath);
    std::vector<std::string> sceneNames;
    for (std::vector<std::string>::const_iterator it = filenames.begin();
    it != filenames.end();
        ++it)
    {
        const std::string& fn = *it;
        if (fn.empty())
            continue;
        if (fn.length() < 4)
            continue;
        const std::string suffix = fn.substr(fn.length() - 4, 4);
        if (!suffix.compare(".lua"))
        {
            const std::string base = fn.substr(0, fn.length() - 4);
            sceneNames.push_back(fn);
        }
    }

    return sceneNames;
}

void initAnt()
{
#ifdef USE_ANTTWEAKBAR
    ///@note Bad size errors will be thrown if this is not called before bar creation.
    TwWindowSize(g_mirrorWindowSz.x, g_mirrorWindowSz.y);

    // Create a tweak bar
    g_pTweakbar = TwNewBar("TweakBar");
    g_pShaderTweakbar = TwNewBar("ShaderTweakBar");

    TwDefine(" GLOBAL fontsize=3 ");
    TwDefine(" TweakBar size='300 580' ");
    TwDefine(" TweakBar position='10 10' ");
    TwDefine(" ShaderTweakBar size='300 420' ");
    TwDefine(" ShaderTweakBar position='290 170' ");

    TwAddVarRW(g_pTweakbar, "FBO Scale", TW_TYPE_FLOAT, &m_fboScale,
        " min=0.05 max=1.0 step=0.005 group='Performance' ");
    TwAddVarRW(g_pTweakbar, "Cinemascope", TW_TYPE_FLOAT, &m_cinemaScope,
        " min=0.05 max=1.0 step=0.005 group='Performance' ");

#endif
}

#ifdef USE_ANTTWEAKBAR
static std::vector<std::string> s_filenames;
static void TW_CALL GoToSceneCB(void *clientData)
{
    if (clientData == NULL)
        return;
    const std::string* pSceneName = reinterpret_cast<const std::string*>(clientData);
    const std::string& sceneName = *pSceneName;

    if (g_pScene != NULL)
    {
#ifdef USE_LUAJIT
        LuajitScene* pLJS = dynamic_cast<LuajitScene*>(g_pScene);
        if (pLJS != NULL)
        {
            pLJS->SetSceneName(sceneName);
        }
#endif
    }
}
#endif

void PopulateSceneTweakbar()
{
#ifdef USE_ANTTWEAKBAR
    TwRemoveAllVars(g_pShaderTweakbar);

    int i = 0;
    s_filenames = getLuaSceneNames();
    for (std::vector<std::string>::const_iterator it = s_filenames.begin();
        it != s_filenames.end();
        ++it, ++i)
    {
        const std::string& fn = *it;
        TwAddButton(g_pShaderTweakbar, fn.c_str(), GoToSceneCB, (void*)(&s_filenames[i]), "  ");
    }
#endif
}

void SwitchScene(int incr)
{
    const std::vector<std::string> sceneNames = getLuaSceneNames();
    g_sceneIdx += incr;
    g_sceneIdx += sceneNames.size();
    g_sceneIdx %= sceneNames.size();
    const std::string& sceneName = sceneNames[g_sceneIdx];

    if (g_pScene != NULL)
    {
#ifdef USE_LUAJIT
        LuajitScene* pLJS = dynamic_cast<LuajitScene*>(g_pScene);
        if (pLJS != NULL)
        {
            pLJS->SetSceneName(sceneName);
        }
#endif
    }

}

glm::mat4 makeWorldToChassisMatrix()
{
    return makeChassisMatrix_glm(m_chassisYaw, 0.f, 0.f, m_chassisPos);
}

// Display the old-fashioned way, to a monoscopic viewport on a desktop monitor.
void displayMonitor()
{
    if (g_pScene == NULL)
        return;

    const glm::mat4 mview = makeWorldToChassisMatrix();
    const glm::ivec2 vp = g_mirrorWindowSz;
    const glm::mat4 persp = glm::perspective(
        90.f,
        static_cast<float>(vp.x) / static_cast<float>(vp.y),
        .004f,
        500.f);
    glViewport(0, 0, vp.x, vp.y);
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDisable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    g_pScene->RenderForOneEye(glm::value_ptr(glm::inverse(mview)), glm::value_ptr(persp));

#ifdef USE_ANTTWEAKBAR
    TwDraw();
#endif
}

static void ErrorCallback(int p_Error, const char* p_Description)
{
    (void)p_Error;
    (void)p_Description;
    LOG_INFO("ERROR: %d, %s", p_Error, p_Description);
}

void keyboard(GLFWwindow* pWindow, int key, int codes, int action, int mods)
{
    (void)pWindow;
    (void)codes;

    if ((key > -1) && (key <= GLFW_KEY_LAST))
    {
        m_keyStates[key] = action;
    }

    if (action == GLFW_PRESS)
    {
    switch (key)
    {
        default:
            if (g_pScene != NULL)
            {
                g_pScene->keypressed(key, codes, action, mods);
            }
            break;

        case GLFW_KEY_W:
        case GLFW_KEY_A:
        case GLFW_KEY_S:
        case GLFW_KEY_D:
            break;

        case GLFW_KEY_R:
            m_chassisPos = glm::vec3(0.f);
            break;

        case GLFW_KEY_F5:
        {
            if (g_pScene != NULL)
            {
                g_pScene->exitGL();
                delete g_pScene;
            }
#ifdef USE_LUAJIT
            g_pScene = new LuajitScene();
#else
            g_pScene = new Scene();
#endif
            if (g_pScene != NULL)
            {
                g_pScene->initGL();
            }
        }
        break;

        case GLFW_KEY_TAB:
            if (mods & GLFW_MOD_CONTROL)
            {
                const int incr = mods & GLFW_MOD_SHIFT ? -1 : 1;
                SwitchScene(incr);
            }
            break;

        case GLFW_KEY_ESCAPE:
            glfwSetWindowShouldClose(g_pMirrorWindow, 1);
            break;
    }
    }
    else if (action == GLFW_RELEASE)
    {
        switch (key)
        {
        default:
            break;
        }
    }

    // Handle keyboard movement(WASD keys)
    const glm::vec3 forward(0.f, 0.f, -1.f);
    const glm::vec3 up(0.f, 1.f, 0.f);
    const glm::vec3 right(1.f, 0.f, 0.f);
    glm::vec3 keyboardMove(0.f);
    float keyboardYaw = 0.f;
    if (m_keyStates['W'] != GLFW_RELEASE) { keyboardMove += forward; }
    if (m_keyStates['S'] != GLFW_RELEASE) { keyboardMove -= forward; }
    if (m_keyStates['A'] != GLFW_RELEASE) { keyboardMove -= right; }
    if (m_keyStates['D'] != GLFW_RELEASE) { keyboardMove += right; }
    if (m_keyStates['Q'] != GLFW_RELEASE) { keyboardMove -= up; }
    if (m_keyStates['E'] != GLFW_RELEASE) { keyboardMove += up; }
    if (m_keyStates[GLFW_KEY_UP] != GLFW_RELEASE) { keyboardMove += forward; }
    if (m_keyStates[GLFW_KEY_DOWN] != GLFW_RELEASE) { keyboardMove -= forward; }
    if (m_keyStates[GLFW_KEY_LEFT] != GLFW_RELEASE) { keyboardMove -= right; }
    if (m_keyStates[GLFW_KEY_RIGHT] != GLFW_RELEASE) { keyboardMove += right; }
    if (m_keyStates['1'] != GLFW_RELEASE) { keyboardYaw -= 1.f; }
    if (m_keyStates['3'] != GLFW_RELEASE) { keyboardYaw += 1.f; }

    float mag = 1.f;
    if (m_keyStates[GLFW_KEY_LEFT_SHIFT] != GLFW_RELEASE)
        mag *= .1f;
    if (m_keyStates[GLFW_KEY_LEFT_CONTROL] != GLFW_RELEASE)
        mag *= 10.f;
    m_keyboardMove = mag * keyboardMove;
    m_keyboardYaw = mag * keyboardYaw;
}

void mouseDown(GLFWwindow* pWindow, int button, int action, int mods)
{
    (void)pWindow;
    (void)mods;

#ifdef USE_ANTTWEAKBAR
    if (action == GLFW_PRESS)
    {
        TwMouseButton(TW_MOUSE_PRESSED, TW_MOUSE_LEFT);
    }
    else if (action == GLFW_RELEASE)
    {
        TwMouseButton(TW_MOUSE_RELEASED, TW_MOUSE_LEFT);
    }
#endif
}

void mouseMove(GLFWwindow* pWindow, double xd, double yd)
{
    glfwGetCursorPos(pWindow, &xd, &yd);
    const int x = static_cast<int>(xd);
    const int y = static_cast<int>(yd);
#ifdef USE_ANTTWEAKBAR
    TwMouseMotion(x, y);
#endif
}

void timestep()
{
    const double absT = g_timer.seconds();
    const double dt = absT - g_lastFrameTime;
    g_lastFrameTime = absT;
    if (g_pScene != NULL)
    {
        g_pScene->timestep(absT, dt);
    }

    // Move in the direction the viewer is facing.
    const glm::vec3 move_dt = m_keyboardMove * static_cast<float>(dt);
    const glm::mat4 moveTxfm =
        makeWorldToChassisMatrix();
    const glm::vec4 mv4 = moveTxfm * glm::vec4(move_dt, 0.f);
    m_chassisPos += glm::vec3(mv4);

    {
        const float rotSpeed = 10.f;
        m_chassisYaw += m_keyboardYaw * static_cast<float>(dt);
    }

    if (g_pScene != NULL)
    {
#ifdef USE_SIXENSE
        const int maxBases = sixenseGetMaxBases();
        for (int base = 0; base < maxBases; ++base)
        {
            sixenseSetActiveBase(base);
            if (!sixenseIsBaseConnected(base))
                continue;
            ///@todo Handle Multiple bases
            sixenseAllControllerData acd;
            sixenseGetAllNewestData(&acd);
            const double absT = g_timer.seconds();
            g_pScene->setTracking_Hydra(absT, (void*)&acd);
        }
#endif // USE_SIXENSE
    }
}

void resize(GLFWwindow* pWindow, int w, int h)
{
    (void)pWindow;
    g_mirrorWindowSz.x = w;
    g_mirrorWindowSz.y = h;
#ifdef USE_ANTTWEAKBAR
    TwWindowSize(w, h);
#endif
}

// OpenGL debug callback
void GLAPIENTRY myCallback(
    GLenum source, GLenum type, GLuint id, GLenum severity,
    GLsizei length, const GLchar *msg,
    const void *data)
{
    switch (severity)
    {
    case GL_DEBUG_SEVERITY_HIGH:
    case GL_DEBUG_SEVERITY_MEDIUM:
    case GL_DEBUG_SEVERITY_LOW:
        LOG_INFO("[[GL Debug]] %x %x %x %x %s", source, type, id, severity, msg);
        break;
    case GL_DEBUG_SEVERITY_NOTIFICATION:
        break;
    }
}

int main(int argc, char** argv)
{
    glfwSetErrorCallback(ErrorCallback);
    if (!glfwInit())
    {
        exit(EXIT_FAILURE);
    }
    glfwWindowHint(GLFW_DEPTH_BITS, 16);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);// : GLFW_OPENGL_COMPAT_PROFILE);
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#ifdef _DEBUG
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
#endif

    GLFWwindow* l_Window = glfwCreateWindow(g_mirrorWindowSz.x, g_mirrorWindowSz.y, "Riftskel window", NULL, NULL);
    if (!l_Window)
    {
        LOG_ERROR("Glfw failed to create a window. Exiting.");
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    glfwMakeContextCurrent(l_Window);
    glfwSetKeyCallback(l_Window, keyboard);
    glfwSetMouseButtonCallback(l_Window, mouseDown);
    glfwSetCursorPosCallback(l_Window, mouseMove);
    glfwSetWindowSizeCallback(l_Window, resize);
    g_pMirrorWindow = l_Window;
#ifdef USE_ANTTWEAKBAR
    TwWindowSize(g_mirrorWindowSz.x, g_mirrorWindowSz.y);
#endif

    memset(m_keyStates, 0, GLFW_KEY_LAST*sizeof(int));

    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK)
    {
        LOG_INFO("glewInit() error.");
        exit(EXIT_FAILURE);
    }

#ifdef _DEBUG
    // Debug callback initialization
    // Must be done *after* glew initialization.
    glDebugMessageCallback(myCallback, NULL);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_TRUE);
    glDebugMessageInsert(GL_DEBUG_SOURCE_APPLICATION, GL_DEBUG_TYPE_MARKER, 0,
        GL_DEBUG_SEVERITY_NOTIFICATION, -1 , "Start debugging");
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
#endif

#ifdef USE_LUAJIT
    g_pScene = new LuajitScene();
#else
    g_pScene = new Scene();
#endif
    if (g_pScene != NULL)
    {
#ifdef USE_LUAJIT
        LuajitScene* pLJS = reinterpret_cast<LuajitScene*>(g_pScene);
        if (pLJS != NULL)
        {
            pLJS->m_pLoaderFunc = (void*)&glfwGetProcAddress;
        }
#endif
        g_pScene->initGL();
    }

#ifdef USE_ANTTWEAKBAR
    TwInit(TW_OPENGL_CORE, NULL);
    initAnt();
    PopulateSceneTweakbar();
#endif

#ifdef USE_SIXENSE
    sixenseInit();
#endif // USE_SIXENSE
    glfwSwapInterval(0);
    while (!glfwWindowShouldClose(l_Window))
    {
        glfwPollEvents();
        timestep();

        glClearColor(1.0, 0.0, 0.0, 0.0);
        glClear(GL_COLOR_BUFFER_BIT);
        displayMonitor();
        glfwSwapBuffers(l_Window);
    }
    g_pScene->exitGL();
#ifdef USE_SIXENSE
    sixenseExit();
#endif // USE_SIXENSE
    glfwDestroyWindow(l_Window);
    glfwTerminate();
    exit(EXIT_SUCCESS);
}
