// main_glfw_ovrsdk08.cpp
// With humongous thanks to cThrough 2014 (Daniel Dekkers)
// Get a window created with GL context and OVR backend initialized,
// then hand off display to the Scene class.

#include <GL/glew.h>
#if defined(_WIN32)
#  include <Windows.h>
#endif
#include <GLFW/glfw3.h>

#include <OVR_CAPI.h>
#include <OVR_CAPI_GL.h>
#include <OVR_ErrorCode.h>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <stdio.h>
#include <string.h>
#include <vector>
#include "FBO.h"
#include "Timer.h"
#include "Logger.h"
#include "Scene.h"
#ifdef USE_LUAJIT
#include "LuajitScene.h"
#endif
#ifdef USE_ANTTWEAKBAR
#  include <AntTweakBar.h>
#endif
#ifdef USE_SIXENSE
#include <sixense.h>
#include <sixense_utils/controller_manager/controller_manager.hpp>
#endif // USE_SIXENSE

#include "HudQuad.h"
#include "AntQuad.h"
#include "DirectoryFunctions.h"
#include "MatrixFunctions.h"

Timer g_timer;
double g_lastFrameTime = 0.0;
GLFWwindow* g_pMirrorWindow = nullptr;
glm::ivec2 g_mirrorWindowSz(1200, 900);

// OVR variables
ovrSession g_session;
ovrHmdDesc m_Hmd;
long long g_frameIndex = 0;
bool g_hmdVisible = false;

ovrTextureSwapChain g_textureSwapChain[ovrEye_Count];
ovrPosef m_eyePoses[ovrEye_Count]; // hold on to these for use outside of draw func
FBO m_swapFBO[ovrEye_Count];

ovrMirrorTexture g_mirrorTexture = nullptr;
FBO m_mirrorFBO;
ovrPerfHudMode m_perfHudMode = ovrPerfHud_Off;
ovrInputState lastRemoteInputState = { 0 };
ovrInputState lastXboxControllerInputState = { 0 };

// Scene variables
IScene* g_pScene = nullptr;
AntQuad g_tweakbarQuad;
#ifdef USE_ANTTWEAKBAR
TwBar* g_pTweakbar = nullptr;
TwBar* g_pShaderTweakbar = nullptr;
#endif

float m_fboScale = 1.f;
float m_cinemaScope = 0.f;
float m_clearColor[3] = { .3f, .3f, .3f };

int m_keyStates[GLFW_KEY_LAST];
glm::vec3 m_keyboardMove(0.f);
float m_keyboardYaw = 0.f;
const glm::vec3 g_chassisDefaultPos(0.f, 1.f, .8f);
glm::vec3 m_chassisPos(g_chassisDefaultPos);
float m_chassisYaw = 0.f;

void reinitScene()
{
    if (g_pScene != NULL)
    {
        g_pScene->exitGL();
        delete g_pScene;
    }
#ifdef USE_LUAJIT
    g_pScene = new LuajitScene();
#else
    g_pScene = new Scene();
#endif
    if (g_pScene != NULL)
    {
        g_pScene->initGL();
    }
}

void initAnt()
{
#ifdef USE_ANTTWEAKBAR
    ///@note Bad size errors will be thrown if this is not called before bar creation.
    TwWindowSize(g_mirrorWindowSz.x, g_mirrorWindowSz.y);

    // Create a tweak bar
    g_pTweakbar = TwNewBar("TweakBar");
    g_pShaderTweakbar = TwNewBar("ShaderTweakBar");

    TwDefine(" GLOBAL fontsize=3 ");
    TwDefine(" TweakBar size='300 580' ");
    TwDefine(" TweakBar position='10 10' ");
    TwDefine(" ShaderTweakBar size='300 420' ");
    TwDefine(" ShaderTweakBar position='290 170' ");

    TwAddVarRW(g_pTweakbar, "FBO Scale", TW_TYPE_FLOAT, &m_fboScale,
        " min=0.05 max=1.0 step=0.005 group='Performance' ");
    TwAddVarRW(g_pTweakbar, "Cinemascope", TW_TYPE_FLOAT, &m_cinemaScope,
        " min=0.05 max=1.0 step=0.005 group='Performance' ");
    TwAddVarRW(g_pTweakbar, "Clear Color", TW_TYPE_COLOR3F, &m_clearColor,
        " group='Appearance' ");
#endif
}

#ifdef USE_ANTTWEAKBAR
static std::vector<std::string> s_filenames;
static void TW_CALL GoToSceneCB(void *clientData)
{
    if (clientData == NULL)
        return;
    const std::string* pSceneName = reinterpret_cast<const std::string*>(clientData);
    const std::string& sceneName = *pSceneName;

    if (g_pScene != NULL)
    {
#ifdef USE_LUAJIT
        LuajitScene* pLJS = dynamic_cast<LuajitScene*>(g_pScene);
        if (pLJS != NULL)
        {
            const int ret = pLJS->SetSceneName(sceneName);
            if (ret != 0)
            {
                // Scene creation failed
                ///@todo maybe remove this entry from the tweakbar?
                LOG_ERROR("LuajitScene::SetSceneName failed with code %d", ret);
                reinitScene();
            }
        }
#endif
    }
}
#endif

void PopulateSceneTweakbar()
{
#ifdef USE_ANTTWEAKBAR
    TwRemoveAllVars(g_pShaderTweakbar);

    const std::string luaPath = "../lua/scene2/";
    int i = 0;
    s_filenames = GetListOfFilesFromDirectory(luaPath);
    for (std::vector<std::string>::const_iterator it = s_filenames.begin();
        it != s_filenames.end();
        ++it, ++i)
    {
        const std::string& fn = *it;
        if (fn.empty())
            continue;
        if (fn.length() < 4)
            continue;
        const std::string suffix = fn.substr(fn.length() - 4, 4);
        if (!suffix.compare(".lua"))
        {
            const std::string base = fn.substr(0, fn.length() - 4);
            TwAddButton(g_pShaderTweakbar, fn.c_str(), GoToSceneCB, (void*)(&s_filenames[i]), "  ");
        }
    }
#endif
}

///@brief Can be called before GL context is initialized.
void initHMD()
{
    ovrResult result = ovr_Initialize(nullptr);
    if (OVR_FAILURE(result))
    {
        LOG_ERROR("ovr_Initialize failed with code %d", result);
        return;
    }

    ovrGraphicsLuid luid;
    result = ovr_Create(&g_session, &luid);
    if (OVR_FAILURE(result))
    {
        ovr_Shutdown();
        LOG_ERROR("ovr_Create failed with code %d", result);
        return;
    }

    ovrSessionStatus sessionStatus;
    ovr_GetSessionStatus(g_session, &sessionStatus);
    if (sessionStatus.HmdPresent == false)
    {
        LOG_ERROR("No HMD Present.");
        return;
    }

    m_Hmd = ovr_GetHmdDesc(g_session);
}

///@brief Called once a GL context has been set up.
void initVR()
{
    const ovrHmdDesc& hmd = m_Hmd;

    for (int eye = 0; eye < 2; ++eye)
    {
        const ovrSizei& bufferSize = ovr_GetFovTextureSize(g_session, ovrEyeType(eye), hmd.DefaultEyeFov[eye], 1.f);
        LOG_INFO("Eye %d tex : %dx%d @ ()", eye, bufferSize.w, bufferSize.h);

        ovrTextureSwapChain textureSwapChain = 0;
        ovrTextureSwapChainDesc desc = {};
        desc.Type = ovrTexture_2D;
        desc.ArraySize = 1;
        desc.Format = OVR_FORMAT_R8G8B8A8_UNORM_SRGB;
        desc.Width = bufferSize.w;
        desc.Height = bufferSize.h;
        desc.MipLevels = 1;
        desc.SampleCount = 1;
        desc.StaticImage = ovrFalse;

        // Allocate the frameBuffer that will hold the scene, and then be
        // re-rendered to the screen with distortion
        ovrTextureSwapChain& chain = g_textureSwapChain[eye];
        if (ovr_CreateTextureSwapChainGL(g_session, &desc, &chain) == ovrSuccess)
        {
            int length = 0;
            ovr_GetTextureSwapChainLength(g_session, chain, &length);

            for (int i = 0; i < length; ++i)
            {
                GLuint chainTexId;
                ovr_GetTextureSwapChainBufferGL(g_session, chain, i, &chainTexId);
                glBindTexture(GL_TEXTURE_2D, chainTexId);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            }
        }
        else
        {
            LOG_ERROR("Unable to create swap textures");
            return;
        }

        // Manually assemble swap FBO
        FBO& swapfbo = m_swapFBO[eye];
        swapfbo.w = bufferSize.w;
        swapfbo.h = bufferSize.h;
        glGenFramebuffers(1, &swapfbo.id);
        glBindFramebuffer(GL_FRAMEBUFFER, swapfbo.id);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, swapfbo.tex, 0);

        swapfbo.depth = 0;
        glGenRenderbuffers(1, &swapfbo.depth);
        glBindRenderbuffer(GL_RENDERBUFFER, swapfbo.depth);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, bufferSize.w, bufferSize.h);
        glBindRenderbuffer(GL_RENDERBUFFER, 0);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, swapfbo.depth);

        // Check status
        const GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        if (status != GL_FRAMEBUFFER_COMPLETE)
        {
            LOG_ERROR("Framebuffer status incomplete: %d %x", status, status);
        }
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    // Initialize mirror texture
    ovrMirrorTextureDesc desc;
    memset(&desc, 0, sizeof(desc));
    desc.Width = g_mirrorWindowSz.x;
    desc.Height = g_mirrorWindowSz.y;
    desc.Format = OVR_FORMAT_R8G8B8A8_UNORM_SRGB;

    const ovrResult result = ovr_CreateMirrorTextureGL(g_session, &desc, &g_mirrorTexture);
    if (!OVR_SUCCESS(result))
    {
        LOG_ERROR("Unable to create mirror texture");
        return;
    }

    // Manually assemble mirror FBO
    m_mirrorFBO.w = g_mirrorWindowSz.x;
    m_mirrorFBO.h = g_mirrorWindowSz.y;
    glGenFramebuffers(1, &m_mirrorFBO.id);
    glBindFramebuffer(GL_FRAMEBUFFER, m_mirrorFBO.id);
    GLuint texId;
    ovr_GetMirrorTextureBufferGL(g_session, g_mirrorTexture, &texId);
    m_mirrorFBO.tex = texId;
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_mirrorFBO.tex, 0);

    const ovrSizei sz = { 600, 600 };
    g_tweakbarQuad.initGL(g_session, sz);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    g_hmdVisible = true;
}

glm::mat4 makeWorldToChassisMatrix()
{
    return makeChassisMatrix_glm(m_chassisYaw, 0.f, 0.f, m_chassisPos);
}

// Display the old-fashioned way, to a monoscopic viewport on a desktop monitor.
void displayMonitor()
{
    if (g_pScene == NULL)
        return;

    glm::mat4 mview = makeWorldToChassisMatrix();
    // This offset vector is an attempt to line up the monitor viewpoint
    // with approximately the "center eye" of VR viewpoint.
    const glm::vec3 sittingOffset(-.25f, .55f, 0.f);
    mview = glm::translate(mview, sittingOffset);

    const glm::ivec2 vp = g_mirrorWindowSz;
    const glm::mat4 persp = glm::perspective(
        90.f,
        static_cast<float>(vp.x) / static_cast<float>(vp.y),
        .004f,
        500.f);
    glViewport(0, 0, vp.x, vp.y);
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDisable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    g_pScene->RenderForOneEye(glm::value_ptr(glm::inverse(mview)), glm::value_ptr(persp));

#ifdef USE_ANTTWEAKBAR
    TwDraw();
#endif
}

// Display to an HMD with OVR SDK backend.
void displayHMD()
{
    ovrSessionStatus sessionStatus;
    ovr_GetSessionStatus(g_session, &sessionStatus);

    if (sessionStatus.HmdPresent == false)
    {
        displayMonitor();
        return;
    }

    const ovrHmdDesc& hmdDesc = m_Hmd;
    double sensorSampleTime; // sensorSampleTime is fed into the layer later
    if (g_hmdVisible)
    {
        // Call ovr_GetRenderDesc each frame to get the ovrEyeRenderDesc, as the returned values (e.g. HmdToEyeOffset) may change at runtime.
        ovrEyeRenderDesc eyeRenderDesc[2];
        eyeRenderDesc[0] = ovr_GetRenderDesc(g_session, ovrEye_Left, hmdDesc.DefaultEyeFov[0]);
        eyeRenderDesc[1] = ovr_GetRenderDesc(g_session, ovrEye_Right, hmdDesc.DefaultEyeFov[1]);

        // Get eye poses, feeding in correct IPD offset
        ovrVector3f HmdToEyeOffset[2] = {
            eyeRenderDesc[0].HmdToEyeOffset,
            eyeRenderDesc[1].HmdToEyeOffset };
#if 0
        // Get both eye poses simultaneously, with IPD offset already included.
        double displayMidpointSeconds = ovr_GetPredictedDisplayTime(g_session, 0);
        ovrTrackingState hmdState = ovr_GetTrackingState(g_session, displayMidpointSeconds, ovrTrue);
        ovr_CalcEyePoses(hmdState.HeadPose.ThePose, HmdToEyeOffset, m_eyePoses);
#else
        ovr_GetEyePoses(g_session, g_frameIndex, ovrTrue, HmdToEyeOffset, m_eyePoses, &sensorSampleTime);
#endif

        for (int eye = 0; eye < 2; ++eye)
        {
            const FBO& swapfbo = m_swapFBO[eye];
            const ovrTextureSwapChain& chain = g_textureSwapChain[eye];

            int curIndex;
            ovr_GetTextureSwapChainCurrentIndex(g_session, chain, &curIndex);
            GLuint curTexId;
            ovr_GetTextureSwapChainBufferGL(g_session, chain, curIndex, &curTexId);

            glBindFramebuffer(GL_FRAMEBUFFER, swapfbo.id);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, curTexId, 0);

            glViewport(0, 0, swapfbo.w, swapfbo.h);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glEnable(GL_FRAMEBUFFER_SRGB);

            {
                glClearColor(m_clearColor[0], m_clearColor[1], m_clearColor[2], 0.f);
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

                const ovrSizei& downSize = ovr_GetFovTextureSize(g_session, ovrEyeType(eye), hmdDesc.DefaultEyeFov[eye], m_fboScale);
                ovrRecti vp = { 0, 0, downSize.w, downSize.h };
                const int texh = swapfbo.h;
                vp.Pos.y = (texh - vp.Size.h) / 2;
                glViewport(vp.Pos.x, vp.Pos.y, vp.Size.w, vp.Size.h);

                // Cinemascope - letterbox bars scissoring off pixels above and below vp center
                const float hc = .5f * m_cinemaScope;
                const int scisPx = static_cast<int>(hc * static_cast<float>(vp.Size.h));
                ovrRecti sp = vp;
                sp.Pos.y += scisPx;
                sp.Size.h -= 2 * scisPx;
                glScissor(sp.Pos.x, sp.Pos.y, sp.Size.w, sp.Size.h);
                glEnable(GL_SCISSOR_TEST);
                glEnable(GL_DEPTH_TEST);

                // Render the scene for the current eye
                const ovrPosef& eyePose = m_eyePoses[eye];
                const glm::mat4 mview =
                    makeWorldToChassisMatrix() *
                    makeMatrixFromPose(eyePose);
                const ovrMatrix4f ovrproj = ovrMatrix4f_Projection(hmdDesc.DefaultEyeFov[eye], 0.2f, 1000.0f, ovrProjection_None);
                const glm::mat4 proj = makeGlmMatrixFromOvrMatrix(ovrproj);
                g_pScene->RenderForOneEye(glm::value_ptr(glm::inverse(mview)), glm::value_ptr(proj));

                const ovrTextureSwapChain& chain = g_textureSwapChain[eye];
                ovr_CommitTextureSwapChain(g_session, chain);
            }
            glDisable(GL_SCISSOR_TEST);

            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, 0, 0);
            glBindFramebuffer(GL_FRAMEBUFFER, 0);
        }
    }

    std::vector<const ovrLayerHeader*> layerHeaders;
    {
        // Do distortion rendering, Present and flush/sync
        ovrLayerEyeFov ld;
        ld.Header.Type = ovrLayerType_EyeFov;
        ld.Header.Flags = ovrLayerFlag_TextureOriginAtBottomLeft; // Because OpenGL.

        for (int eye = 0; eye < 2; ++eye)
        {
            const FBO& swapfbo = m_swapFBO[eye];
            const ovrTextureSwapChain& chain = g_textureSwapChain[eye];

            ld.ColorTexture[eye] = chain;

            const ovrSizei& downSize = ovr_GetFovTextureSize(g_session, ovrEyeType(eye), hmdDesc.DefaultEyeFov[eye], m_fboScale);
            ovrRecti vp = { 0, 0, downSize.w, downSize.h };
            const int texh = swapfbo.h;
            vp.Pos.y = (texh - vp.Size.h) / 2;

            ld.Viewport[eye] = vp;
            ld.Fov[eye] = hmdDesc.DefaultEyeFov[eye];
            ld.RenderPose[eye] = m_eyePoses[eye];
            ld.SensorSampleTime = sensorSampleTime;
        }
        layerHeaders.push_back(&ld.Header);

        // Submit layers to HMD for display
        ovrLayerQuad ql;
        if (g_tweakbarQuad.m_showQuadInWorld)
        {
            ql.Header.Type = ovrLayerType_Quad;
            ql.Header.Flags = ovrLayerFlag_TextureOriginAtBottomLeft; // Because OpenGL.

            ql.ColorTexture = g_tweakbarQuad.m_swapChain;
            ovrRecti vp;
            vp.Pos.x = 0;
            vp.Pos.y = 0;
            vp.Size.w = 600; ///@todo
            vp.Size.h = 600; ///@todo
            ql.Viewport = vp;
            ql.QuadPoseCenter = g_tweakbarQuad.m_QuadPoseCenter;
            ql.QuadSize = { 1.f, 1.f }; ///@todo Pass in

            g_tweakbarQuad.SetHmdEyeRay(m_eyePoses[ovrEyeType::ovrEye_Left]); // Writes to m_layerQuad.QuadPoseCenter
            g_tweakbarQuad.DrawToQuad();
            layerHeaders.push_back(&ql.Header);
        }
    }

#if 0
    ovrViewScaleDesc viewScaleDesc;
    viewScaleDesc.HmdToEyeOffset[0] = m_eyeOffsets[0];
    viewScaleDesc.HmdToEyeOffset[1] = m_eyeOffsets[1];
    viewScaleDesc.HmdSpaceToWorldScaleInMeters = 1.f;
#endif

    const ovrResult result = ovr_SubmitFrame(g_session, g_frameIndex, nullptr, &layerHeaders[0], layerHeaders.size());
    if (result == ovrSuccess)
    {
        g_hmdVisible = true;
    }
    else if (result == ovrSuccess_NotVisible)
    {
        g_hmdVisible = false;
        ///@todo Enter a lower-power, polling "no focus/HMD not worn" mode
    }
    else if (result == ovrError_DisplayLost)
    {
        LOG_INFO("ovr_SubmitFrame returned ovrError_DisplayLost");
        g_hmdVisible = false;
        ///@todo Tear down textures and session and re-create
    }

    // Handle OVR session events
    ovr_GetSessionStatus(g_session, &sessionStatus);
    if (sessionStatus.ShouldQuit)
    {
        glfwSetWindowShouldClose(g_pMirrorWindow, 1);
    }
    if (sessionStatus.ShouldRecenter)
    {
        ovr_RecenterTrackingOrigin(g_session);
    }

    // Blit mirror texture to monitor window
    if (g_hmdVisible)
    {
        glViewport(0, 0, g_mirrorWindowSz.x, g_mirrorWindowSz.y);
        const FBO& srcFBO = m_mirrorFBO;
        glBindFramebuffer(GL_READ_FRAMEBUFFER, srcFBO.id);
        glBlitFramebuffer(
            0, srcFBO.h, srcFBO.w, 0,
            0, 0, g_mirrorWindowSz.x, g_mirrorWindowSz.y,
            GL_COLOR_BUFFER_BIT, GL_NEAREST);
        glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
    }
    else
    {
        displayMonitor();
    }
    ++g_frameIndex;
}

void exitVR()
{
    ///@todo delete swap fbos
    //_DestroySwapTextures();

    for (int eye = 0; eye < 2; ++eye)
    {
        ovrTextureSwapChain& chain = g_textureSwapChain[eye];
        ovr_DestroyTextureSwapChain(g_session, chain);
    }

    ovr_Destroy(g_session);
    ovr_Shutdown();
}

static void ErrorCallback(int p_Error, const char* p_Description)
{
    (void)p_Error;
    (void)p_Description;
    LOG_INFO("ERROR: %d, %s", p_Error, p_Description);
}

void keyboard(GLFWwindow* pWindow, int key, int scancode, int action, int mods)
{
    (void)pWindow;

    bool ret = false;
    if (g_pScene != NULL)
    {
        switch (key)
        {
        case GLFW_KEY_BACKSPACE:
        case GLFW_KEY_S:
        case GLFW_KEY_LEFT:
        case GLFW_KEY_RIGHT:
        case GLFW_KEY_UP:
        case GLFW_KEY_DOWN:
        case GLFW_KEY_PAGE_UP:
        case GLFW_KEY_PAGE_DOWN:
        case GLFW_KEY_ENTER:
            ret = g_pScene->keypressed(key, scancode, action, mods);
            break;

        default:
            break;
        }
    }
    if (ret == true)
    {
        // Event was consumed by Scene
        return;
    }

    if ((key > -1) && (key <= GLFW_KEY_LAST))
    {
        m_keyStates[key] = action;
    }

    if (action == GLFW_PRESS)
    {
    switch (key)
    {
        default:
            break;

        case GLFW_KEY_W:
        case GLFW_KEY_A:
        case GLFW_KEY_S:
        case GLFW_KEY_D:
            break;

        case GLFW_KEY_SPACE:
            ovr_RecenterTrackingOrigin(g_session);
            break;

        case GLFW_KEY_R:
            m_chassisPos = g_chassisDefaultPos;
            m_chassisYaw = 0.f;
            break;

        //case GLFW_KEY_BACKSPACE:
        case GLFW_KEY_BACKSLASH:
            {
                int phm = static_cast<int>(m_perfHudMode);
                ++phm %= static_cast<int>(ovrPerfHud_Count);
                m_perfHudMode = static_cast<ovrPerfHudMode>(phm);
                ovr_SetInt(g_session, OVR_PERF_HUD_MODE, m_perfHudMode);
            }
            break;

        case GLFW_KEY_GRAVE_ACCENT:
        case GLFW_KEY_TAB:
            g_tweakbarQuad.m_showQuadInWorld = !g_tweakbarQuad.m_showQuadInWorld;
            break;

        case GLFW_KEY_ENTER:
            if (g_tweakbarQuad.m_showQuadInWorld)
            {
                g_tweakbarQuad.MouseClick(1);
            }
            else
            {
                g_tweakbarQuad.m_showQuadInWorld = !g_tweakbarQuad.m_showQuadInWorld;
            }
            break;

        case GLFW_KEY_SLASH:
            g_tweakbarQuad.SetHoldingFlag(m_eyePoses[0], true);
            break;

        case GLFW_KEY_F5:
            reinitScene();
            break;

        case GLFW_KEY_ESCAPE:
            glfwSetWindowShouldClose(g_pMirrorWindow, 1);
            break;
    }
    }
    else if (action == GLFW_RELEASE)
    {
        switch (key)
        {
        default:
            break;

        case GLFW_KEY_SLASH:
            g_tweakbarQuad.SetHoldingFlag(m_eyePoses[0], false);
            break;

        case GLFW_KEY_ENTER:
            if (g_tweakbarQuad.m_showQuadInWorld)
            {
                g_tweakbarQuad.MouseClick(0);
            }
            break;
        }
    }

    // Handle keyboard movement(WASD keys)
    const glm::vec3 forward(0.f, 0.f, -1.f);
    const glm::vec3 up(0.f, 1.f, 0.f);
    const glm::vec3 right(1.f, 0.f, 0.f);
    glm::vec3 keyboardMove(0.f);
    float keyboardYaw = 0.f;
    if (m_keyStates['W'] != GLFW_RELEASE) { keyboardMove += forward; }
    if (m_keyStates['S'] != GLFW_RELEASE) { keyboardMove -= forward; }
    if (m_keyStates['A'] != GLFW_RELEASE) { keyboardMove -= right; }
    if (m_keyStates['D'] != GLFW_RELEASE) { keyboardMove += right; }
    if (m_keyStates['Q'] != GLFW_RELEASE) { keyboardMove -= up; }
    if (m_keyStates['E'] != GLFW_RELEASE) { keyboardMove += up; }
    if (m_keyStates[GLFW_KEY_UP] != GLFW_RELEASE) { keyboardMove += forward; }
    if (m_keyStates[GLFW_KEY_DOWN] != GLFW_RELEASE) { keyboardMove -= forward; }
    if (m_keyStates[GLFW_KEY_LEFT] != GLFW_RELEASE) { keyboardMove -= right; }
    if (m_keyStates[GLFW_KEY_RIGHT] != GLFW_RELEASE) { keyboardMove += right; }
    if (m_keyStates[GLFW_KEY_PAGE_UP] != GLFW_RELEASE) { keyboardMove += up; }
    if (m_keyStates[GLFW_KEY_PAGE_DOWN] != GLFW_RELEASE) { keyboardMove -= up; }
    if (m_keyStates['1'] != GLFW_RELEASE) { keyboardYaw -= 1.f; }
    if (m_keyStates['3'] != GLFW_RELEASE) { keyboardYaw += 1.f; }

    float mag = 1.f;
    if (m_keyStates[GLFW_KEY_LEFT_SHIFT] != GLFW_RELEASE)
        mag *= .1f;
    if (m_keyStates[GLFW_KEY_LEFT_CONTROL] != GLFW_RELEASE)
        mag *= 10.f;
    if (m_keyStates[GLFW_KEY_RIGHT_SHIFT] != GLFW_RELEASE)
        mag *= .1f;
    if (m_keyStates[GLFW_KEY_RIGHT_CONTROL] != GLFW_RELEASE)
        mag *= 10.f;
    m_keyboardMove = mag * keyboardMove;
    m_keyboardYaw = mag * keyboardYaw;
}

void charkey(GLFWwindow* pWindow, unsigned int codepoint)
{
    (void)pWindow;

    if (g_pScene != NULL)
    {
        g_pScene->charkeypressed(codepoint);
    }
}

void mouseDown(GLFWwindow* pWindow, int button, int action, int mods)
{
    (void)pWindow;
    (void)mods;

    if ((button == GLFW_MOUSE_BUTTON_MIDDLE) && (action == GLFW_PRESS))
    {
        g_tweakbarQuad.m_showQuadInWorld = !g_tweakbarQuad.m_showQuadInWorld;
        return;
    }
    g_tweakbarQuad.MouseClick(action); ///@todo button id
}

void mouseMove(GLFWwindow* pWindow, double xd, double yd)
{
    glfwGetCursorPos(pWindow, &xd, &yd);
    const int x = static_cast<int>(xd);
    const int y = static_cast<int>(yd);
    g_tweakbarQuad.MouseMotion(x, y);
}

// OVR Remote controller input
void HandleRemote()
{
    ovrInputState currentRemoteInputState;
    ovr_GetInputState(g_session, ovrControllerType_Remote, &currentRemoteInputState);
    const unsigned int b = currentRemoteInputState.Buttons;
    const unsigned int b0 = lastRemoteInputState.Buttons;
    if (b & ovrButton_Enter)
    {
        if (!(b0 & ovrButton_Enter))
        {
            g_tweakbarQuad.MouseClick(1);
        }
    }
    else if (!(b & ovrButton_Enter))
    {
        if (b0 & ovrButton_Enter)
        {
            g_tweakbarQuad.MouseClick(0);
        }
    }
    lastRemoteInputState = currentRemoteInputState;
}

void HandleXboxController()
{
    ovrInputState currentXboxControllerInputState;
    ovr_GetInputState(g_session, ovrControllerType_XBox, &currentXboxControllerInputState);
    const unsigned int b = currentXboxControllerInputState.Buttons;
    const unsigned int b0 = lastXboxControllerInputState.Buttons;
    const int32_t Abut = ovrButton_A;
    if (b & Abut)
    {
        if (!(b0 & Abut))
        {
            g_tweakbarQuad.MouseClick(1);
        }
    }
    else if (!(b & Abut))
    {
        if (b0 & Abut)
        {
            g_tweakbarQuad.MouseClick(0);
        }
    }

    if ((b & ovrButton_Y) && !(b0 & ovrButton_Y))
    {
        g_tweakbarQuad.m_showQuadInWorld = !g_tweakbarQuad.m_showQuadInWorld;
    }
    lastXboxControllerInputState = currentXboxControllerInputState;
}

void timestep()
{
    const double absT = g_timer.seconds();
    const double dt = absT - g_lastFrameTime;
    g_lastFrameTime = absT;
    if (g_pScene != NULL)
    {
        g_pScene->timestep(absT, dt);
    }

    const glm::vec3 move_dt = m_keyboardMove * static_cast<float>(dt);
    glm::mat4 moveTxfm = makeWorldToChassisMatrix();

    // Move in the direction the viewer is facing if HMD is worn.
    ovrSessionStatus sessionStatus;
    ovr_GetSessionStatus(g_session, &sessionStatus);
    if (sessionStatus.HmdMounted == true)
    {
        moveTxfm *= makeMatrixFromPose(m_eyePoses[0]);
    }

    const glm::vec4 mv4 = moveTxfm * glm::vec4(move_dt, 0.f);
    m_chassisPos += glm::vec3(mv4);

    {
        const float rotSpeed = 10.f;
        m_chassisYaw += m_keyboardYaw * static_cast<float>(dt);
    }

    if (g_pScene != NULL)
    {
#ifdef USE_SIXENSE
        const int maxBases = sixenseGetMaxBases();
        for (int base = 0; base < maxBases; ++base)
        {
            sixenseSetActiveBase(base);
            if (!sixenseIsBaseConnected(base))
                continue;
            ///@todo Handle Multiple bases
            sixenseAllControllerData acd;
            sixenseGetAllNewestData(&acd);
            const double absT = g_timer.seconds();
            g_pScene->setTracking_Hydra(absT, (void*)&acd);
        }
#endif // USE_SIXENSE
    }

    HandleRemote();
    HandleXboxController();
}

void resize(GLFWwindow* pWindow, int w, int h)
{
    (void)pWindow;
    g_mirrorWindowSz.x = w;
    g_mirrorWindowSz.y = h;
}

// OpenGL debug callback
void GLAPIENTRY myCallback(
    GLenum source, GLenum type, GLuint id, GLenum severity,
    GLsizei length, const GLchar *msg,
    const void *data)
{
    switch (severity)
    {
    case GL_DEBUG_SEVERITY_HIGH:
    case GL_DEBUG_SEVERITY_MEDIUM:
    case GL_DEBUG_SEVERITY_LOW:
        LOG_INFO("[[GL Debug]] %x %x %x %x %s", source, type, id, severity, msg);
        break;
    case GL_DEBUG_SEVERITY_NOTIFICATION:
        break;
    }
}

int main(int argc, char** argv)
{
    initHMD();

    glfwSetErrorCallback(ErrorCallback);
    if (!glfwInit())
    {
        exit(EXIT_FAILURE);
    }
    glfwWindowHint(GLFW_DEPTH_BITS, 16);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);// : GLFW_OPENGL_COMPAT_PROFILE);
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#ifdef _DEBUG
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
#endif

    GLFWwindow* l_Window = glfwCreateWindow(g_mirrorWindowSz.x, g_mirrorWindowSz.y, "Mirror window", NULL, NULL);
    if (!l_Window)
    {
        LOG_ERROR("Glfw failed to create a window. Exiting.");
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    glfwMakeContextCurrent(l_Window);
    glfwSetKeyCallback(l_Window, keyboard);
    glfwSetCharCallback(l_Window, charkey);
    glfwSetMouseButtonCallback(l_Window, mouseDown);
    glfwSetCursorPosCallback(l_Window, mouseMove);
    glfwSetWindowSizeCallback(l_Window, resize);
    g_pMirrorWindow = l_Window;

    memset(m_keyStates, 0, GLFW_KEY_LAST*sizeof(int));

    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK)
    {
        LOG_INFO("glewInit() error.");
        exit(EXIT_FAILURE);
    }

#ifdef _DEBUG
    // Debug callback initialization
    // Must be done *after* glew initialization.
    glDebugMessageCallback(myCallback, NULL);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_TRUE);
    glDebugMessageInsert(GL_DEBUG_SOURCE_APPLICATION, GL_DEBUG_TYPE_MARKER, 0,
        GL_DEBUG_SEVERITY_NOTIFICATION, -1 , "Start debugging");
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
#endif

#ifdef USE_ANTTWEAKBAR
    TwInit(TW_OPENGL_CORE, NULL);
    initAnt();
    PopulateSceneTweakbar();
#endif

#ifdef USE_LUAJIT
    g_pScene = new LuajitScene();
#else
    g_pScene = new Scene();
#endif
    if (g_pScene != NULL)
    {
#ifdef USE_LUAJIT
        LuajitScene* pLJS = reinterpret_cast<LuajitScene*>(g_pScene);
        if (pLJS != NULL)
        {
            pLJS->m_pLoaderFunc = (void*)&glfwGetProcAddress;
        }
#endif
        g_pScene->initGL();
    }
    initVR();
#ifdef USE_SIXENSE
    sixenseInit();
#endif
    glfwSwapInterval(0);
    while (!glfwWindowShouldClose(l_Window))
    {
        glfwPollEvents();
        timestep();
        displayHMD();
        glfwSwapBuffers(l_Window);
    }
    if (g_pScene != NULL)
    {
        g_pScene->exitGL();
    }
    g_tweakbarQuad.exitGL(g_session);
    exitVR();
#ifdef USE_SIXENSE
    sixenseExit();
#endif
    glfwDestroyWindow(l_Window);
    glfwTerminate();
    exit(EXIT_SUCCESS);
}
